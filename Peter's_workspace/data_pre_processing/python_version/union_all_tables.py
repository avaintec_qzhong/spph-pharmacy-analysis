#! /usr/bin/env python
# -*- coding: utf-8 -*-



import pandas as pd
import os

#数据路径（原始excel）
data_path='E:\\project_data\\武侯\\'
#数据路径（按列合并后并转为csv的数据）
output_path='E:\\project_data\\output\\'

#去除主键字段空值
def del_null_for_primarykey(input_df,key):
    NONE_VIN = (input_df[key].isnull()) | (input_df[key].apply(lambda x: str(x).isspace()))
    return input_df[~NONE_VIN]

#分类函数，将数据文件分类，将相同表结构的数据文件放入一个list中，并转换为绝对路径
def file_partition(source_dir):
    res=[]
    file_names=os.listdir(source_dir)
    types=[file.split('_')[0] for file in file_names]
    types=set(types)
    
    for tp in types:
        mid=[source_dir+file for file in file_names if file.startswith(tp)]
        res.append(mid)
    
    return res


#读取纵向拼接结果（因为每张表的数据量较大，为了避免出现笛卡尔积等问题，将中间结果存下来，避免太多重复操作）
def read_tmp_files(file_path):
    return pd.read_csv(open(file_path,encoding='utf-8'),encoding='utf-8')
       
#读取数据并纵向拼接：将相同表结构的数据纵向拼接，表结构不变
def read_and_merge_files(file_list,output_path,key):
    if len(file_list)==1:
        df_res=pd.read_excel(file_list[0])
        df_res=del_null_for_primarykey(df_res, key)
    else:
        for file in file_list:
            if file_list.index(file)==0:
                df_left=pd.read_excel(file)
                df_left=del_null_for_primarykey(df_left, key)
            else:
                df_right=pd.read_excel(file)
                df_right=del_null_for_primarykey(df_right, key)
                df_res=pd.concat([df_left,df_right])
                df_left=df_res
    df_res=df_res.drop_duplicates()
    file_name=file_list[0].split('\\')[-1].split('_')[0]+'.csv'
    df_res.to_csv(output_path+file_name,encoding='utf-8',index=None)
    return df_res

#数据横向拼接，将两个数据文件按merge_key进行merge操作，操作类型为默认为左关联
def merge_across_all_files(df_left,df_right,merge_key,merge_type='outer'):
    if df_left.dtypes[merge_key]!='int64':
        df_left[[merge_key]]=df_left[[merge_key]].astype('int64')
    if df_right.dtypes[merge_key]!='int64':
        df_right[[merge_key]]=df_right[[merge_key]].astype('int64')
    res=pd.merge(df_left, df_right,how=merge_type,on=merge_key)
    res=res.drop_duplicates()
    return res


def data_pre_processing(data_path):
    
    #读原始文件，合并
    #file_res=file_partition(data_path)
    #df_mid_list=[read_and_merge_files(file,output_path,'患者ID') for file in file_res]
    
    #读处理后的中间结果文件
    file_names=os.listdir(output_path)
    file_res=[output_path+file for file in file_names]
    df_mid_list=[read_tmp_files(file) for file in file_res]
    for index in range(len(df_mid_list)):
        if index==0:
            mid_df=merge_across_all_files(df_mid_list[index], df_mid_list[index+1], '患者ID')
        elif index<len(df_mid_list)-1:
            mid_df=merge_across_all_files(mid_df, df_mid_list[index+1], '患者ID')
        else:
            print('process over,merge file type num:%d'%index)
            mid_df.to_excel(data_path+'whole_data.xlsx',encoding='utf-8',index=None)
            
            
data_pre_processing(data_path)
print()
