#程序功能：加载源表的数据，分析异常值
#加载原始数据
LOAD DATA INFILE 'E:/project_data/output/data/user_info.csv'
INTO TABLE user_info 
FIELDS TERMINATED BY ',' 
IGNORE 1 LINES;


LOAD DATA INFILE 'E:/project_data/output/data/disease_diagnosis.csv'
INTO TABLE disease_diagnosis 
FIELDS TERMINATED BY ','
IGNORE 1 LINES;

LOAD DATA INFILE 'E:/project_data/output/data/sign_package.csv'
INTO TABLE sign_package 
FIELDS TERMINATED BY ',' 
IGNORE 1 LINES;

LOAD DATA INFILE 'E:/project_data/output/data/library_check.csv'
INTO TABLE library_check 
FIELDS TERMINATED BY ',' 
IGNORE 1 LINES;
-----------------------------------------------------------------
异常值:201602009301,DE04.50.172.00,eGFR,2019-04-01,Infinity
201606002200,DE04.50.100.00,肌酐测定,2018-09-05,+1 25
201606012135,DE04.50.037.00,葡萄糖,2018-09-11,-
201613001851,DE04.50.083.00,糖化血红蛋白,2019-06-13,未知
201714002870,DE04.50.100.00,肌酐测定,2018-07-13,-

LOAD DATA INFILE 'E:/project_data/output/data/physical_check.csv'
INTO TABLE physical_check 
FIELDS TERMINATED BY ',' 
IGNORE 1 LINES;
-------------------------------------------------------------------
异常值:201601025127,DE04.10.188.00,体重,2018-09-12,46。5
201904004183,DE04.10.188.00,体重,2019-04-11,54,。5
201801011264,DE04.10.188.00,体重,2019-02-19,68/
201807012171,DE04.10.188.00,体重,2018-07-13,63.8/
201811009543,DE04.10.188.00,体重,2018-11-15,57..5
201904003836,DE04.10.188.00,体重,2019-04-02,52,5
201909001586,DE04.10.188.00,体重,2019-02-15,48..8

LOAD DATA INFILE 'E:/project_data/output/data/smoke_issue.csv'
INTO TABLE smoke_issue 
FIELDS TERMINATED BY ',' 
IGNORE 1 LINES;

LOAD DATA INFILE 'E:/project_data/output/data/drug_using.csv'
INTO TABLE drug_using 
FIELDS TERMINATED BY ',' 
IGNORE 1 LINES;


#加载维表数据
LOAD DATA INFILE 'E:/project_data/dim/dim_diagnosis.csv'
INTO TABLE dim_diagnosis 
FIELDS TERMINATED BY ',' 
IGNORE 1 LINES;

LOAD DATA INFILE 'E:/project_data/dim/dim_check_threshold.csv'
INTO TABLE dim_check_threshold 
FIELDS TERMINATED BY ',' 
IGNORE 1 LINES;

LOAD DATA INFILE 'E:/project_data/dim/dim_drug.csv'
INTO TABLE dim_drug 
FIELDS TERMINATED BY ',' 
IGNORE 1 LINES;
