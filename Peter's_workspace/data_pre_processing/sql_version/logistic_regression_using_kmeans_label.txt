###基于kmeans聚类结果的统计
create table user_id_physical_library_disease as 
select distinct user_id,operate_month,time_interval from 
(select user_id,diagnosis_month operate_month,time_interval from disease_diagnosis_time_interval_final_wide 
union all 
select user_id,using_month operate_month,time_interval from drug_using_count_res_time_interval_wide 
union all 
select user_id,check_month operate_month,time_interval from library_check_if_normal_time_interval_wide 
union all 
select user_id,check_month operate_month,time_interval from physical_check_if_normal_time_interval_wide 
) t

ALTER TABLE user_id_physical_library_disease ADD INDEX user_id_physical_library_disease_index (user_id,operate_month,time_interval);

#对于k值=3的：
drop table if exists library_physical_check_wide_with_clustering_label_3;
CREATE TABLE IF NOT EXISTS library_physical_check_wide_with_clustering_label_3(
   user_id BIGINT comment '患者ID',
   check_date VARCHAR(50) comment '检查日期',
   time_interval varchar(50) DEFAULT NULL COMMENT '时间间隔',
   if_gxxy int comment '高血压',
   if_2tnb int comment '2型糖尿病',
   if_mxzexfjb int comment '慢性阻塞性肺疾病',
   if_gzxz int comment '高脂血症',
   if_zqgxc int comment '支气管哮喘',
   if_gnsxz int comment '高尿酸血症',
   if_gzdmzyyhxxzb int comment '冠状动脉粥样硬化性心脏病',
   if_dzpz int comment '带状疱疹',
   if_fsxgjy int comment '风湿性关节炎',
   if_gzss int comment '骨质疏松',
   if_ncz int comment '脑卒中',
   glucose_res decimal(10,2) comment '葡萄糖指标值',
   ALB_res decimal(10,2) comment '尿微量白蛋白指标值',
   GHb_res decimal(10,2) comment '糖化血红蛋白指标值',
   LDL_res decimal(10,2) comment '低密度脂蛋白胆固醇指标值',
   HDL_res decimal(10,2) comment '高密度脂蛋白胆固醇指标值',
   UCr_res decimal(10,2) comment '肌酐测定指标值',
   TC_res decimal(10,2) comment '总胆固醇指标值',
   eGFR_res decimal(10,2) comment 'eGFR指标值',
   BMI_res decimal(10,2) comment 'BMI指标值',
   SBP_res decimal(10,2) comment '收缩压指标值',
   DBP_res decimal(10,2) comment '舒张压指标值',
   LSBP_res decimal(10,2) comment '左侧收缩压指标值',
   LDBP_res decimal(10,2) comment '左侧舒张压指标值',
   W_res decimal(10,2) comment '腰围指标值',
   H_res decimal(10,2) comment '臀围指标值',
   label BIGINT comment '聚类类别'
)DEFAULT CHARSET=utf8;

ALTER TABLE library_physical_check_wide_with_clustering_label_3 ADD INDEX library_physical_check_wide_with_clustering_label_3_index (user_id,check_date,time_interval);


insert into library_physical_check_wide_with_clustering_label_3 
select t_u.user_id,t_u.operate_month,t_u.time_interval,
if_gxxy,if_2tnb,if_mxzexfjb,if_gzxz,if_zqgxc,if_gnsxz,if_gzdmzyyhxxzb,if_dzpz,if_fsxgjy,if_gzss,if_ncz,
glucose_res,ALB_res,GHb_res,LDL_res,HDL_res,UCr_res,TC_res,eGFR_res,
BMI_res,SBP_res,DBP_res,LSBP_res,LDBP_res,W_res,H_res,
label from 
(select user_id,operate_month,time_interval from user_id_total_behave) t_u 
left outer join 
(select user_id,check_month,time_interval,glucose_res,ALB_res,GHb_res,LDL_res,HDL_res,UCr_res,TC_res,eGFR_res from library_check_if_normal_time_interval_wide) t_lib 
on t_u.user_id=t_lib.user_id and t_u.operate_month=t_lib.check_month and t_u.time_interval=t_lib.time_interval 
left outer join 
(select user_id,check_month,time_interval,BMI_res,SBP_res,DBP_res,LSBP_res,LDBP_res,W_res,H_res 
from physical_check_if_normal_time_interval_wide) t_phy 
on t_u.user_id=t_phy.user_id and t_u.operate_month=t_phy.check_month and t_u.time_interval=t_phy.time_interval 
left join 
(select user_id,if_gxxy,if_2tnb,if_mxzexfjb,if_gzxz,if_zqgxc,if_gnsxz,if_gzdmzyyhxxzb,if_dzpz,if_fsxgjy,if_gzss,if_ncz,label 
from disease_diagnosis_k_means_chosendiag_res_3) t_kmeans
on t_u.user_id=t_kmeans.user_id

#统计
select label,
sum(if_gxxy) sum_if_gxxy,sum(if_2tnb) sum_if_2tnb,sum(if_mxzexfjb) sum_if_mxzexfjb,sum(if_gzxz) sum_if_gzxz,sum(if_zqgxc) sum_if_zqgxc,sum(if_gnsxz) sum_if_gnsxz,sum(if_gzdmzyyhxxzb) sum_if_gzdmzyyhxxzb,sum(if_dzpz) sum_if_dzpz,sum(if_fsxgjy) sum_if_fsxgjy,sum(if_gzss) sum_if_gzss,sum(if_ncz) sum_if_ncz,
avg(avg_glucose_res) avg_glucose_res,avg(avg_ALB_res) avg_ALB_res,avg(avg_GHb_res) avg_GHb_res,avg(avg_LDL_res) avg_LDL_res,avg(avg_HDL_res) avg_HDL_res,avg(avg_UCr_res) avg_UCr_res,avg(avg_TC_res) avg_TC_res ,avg(avg_eGFR_res) avg_eGFR_res,
std(std_glucose_res) std_glucose_res,std(std_ALB_res) std_ALB_res,std(std_GHb_res) std_GHb_res,std(std_LDL_res) std_LDL_res,std(std_HDL_res) std_HDL_res,std(std_UCr_res) std_UCr_res,std(std_TC_res) std_TC_res,std(std_eGFR_res) std_eGFR_res,
avg(avg_BMI_res) avg_BMI_res,avg(avg_SBP_res) avg_SBP_res,avg(avg_DBP_res) avg_DBP_res,avg(avg_LSBP_res) avg_LSBP_res,avg(avg_LDBP_res) avg_LDBP_res,avg(avg_W_res) avg_W_res,avg(avg_H_res) avg_H_res,
std(std_BMI_res) std_BMI_res,std(std_SBP_res) std_SBP_res,std(std_DBP_res) std_DBP_res,std(std_LSBP_res) std_LSBP_res,std(std_LDBP_res) std_LDBP_res,std(std_W_res) std_W_res,std(std_H_res) std_H_res 
from 
(select user_id,ifnull(label,3) label,max(if_gxxy) if_gxxy,max(if_2tnb) if_2tnb,max(if_mxzexfjb) if_mxzexfjb,max(if_gzxz) if_gzxz,max(if_zqgxc) if_zqgxc,max(if_gnsxz) if_gnsxz,max(if_gzdmzyyhxxzb) if_gzdmzyyhxxzb,max(if_dzpz) if_dzpz,max(if_fsxgjy) if_fsxgjy,max(if_gzss) if_gzss,max(if_ncz) if_ncz,
avg(glucose_res) avg_glucose_res,avg(ALB_res) avg_ALB_res,avg(GHb_res) avg_GHb_res,avg(LDL_res) avg_LDL_res,avg(HDL_res) avg_HDL_res,avg(UCr_res) avg_UCr_res,avg(TC_res) avg_TC_res ,avg(eGFR_res) avg_eGFR_res,
std(glucose_res) std_glucose_res,std(ALB_res) std_ALB_res,std(GHb_res) std_GHb_res,std(LDL_res) std_LDL_res,std(HDL_res) std_HDL_res,std(UCr_res) std_UCr_res,std(TC_res) std_TC_res,std(eGFR_res) std_eGFR_res,
avg(BMI_res) avg_BMI_res,avg(SBP_res) avg_SBP_res,avg(DBP_res) avg_DBP_res,avg(LSBP_res) avg_LSBP_res,avg(LDBP_res) avg_LDBP_res,avg(W_res) avg_W_res,avg(H_res) avg_H_res,
std(BMI_res) std_BMI_res,std(SBP_res) std_SBP_res,std(DBP_res) std_DBP_res,std(LSBP_res) std_LSBP_res,std(LDBP_res) std_LDBP_res,std(W_res) std_W_res,std(H_res) std_H_res 
from library_physical_check_wide_with_clustering_label_3 group by user_id,label) t_in 
group by label order by label


#对于k值=5的：
drop table if exists library_physical_check_wide_with_clustering_label_5;
CREATE TABLE IF NOT EXISTS library_physical_check_wide_with_clustering_label_5(
   user_id BIGINT comment '患者ID',
   check_date VARCHAR(50) comment '检查日期',
   time_interval varchar(50) DEFAULT NULL COMMENT '时间间隔',
   if_gxxy int comment '高血压',
   if_2tnb int comment '2型糖尿病',
   if_mxzexfjb int comment '慢性阻塞性肺疾病',
   if_gzxz int comment '高脂血症',
   if_zqgxc int comment '支气管哮喘',
   if_gnsxz int comment '高尿酸血症',
   if_gzdmzyyhxxzb int comment '冠状动脉粥样硬化性心脏病',
   if_dzpz int comment '带状疱疹',
   if_fsxgjy int comment '风湿性关节炎',
   if_gzss int comment '骨质疏松',
   if_ncz int comment '脑卒中',
   glucose_res decimal(10,2) comment '葡萄糖指标值',
   ALB_res decimal(10,2) comment '尿微量白蛋白指标值',
   GHb_res decimal(10,2) comment '糖化血红蛋白指标值',
   LDL_res decimal(10,2) comment '低密度脂蛋白胆固醇指标值',
   HDL_res decimal(10,2) comment '高密度脂蛋白胆固醇指标值',
   UCr_res decimal(10,2) comment '肌酐测定指标值',
   TC_res decimal(10,2) comment '总胆固醇指标值',
   eGFR_res decimal(10,2) comment 'eGFR指标值',
   BMI_res decimal(10,2) comment 'BMI指标值',
   SBP_res decimal(10,2) comment '收缩压指标值',
   DBP_res decimal(10,2) comment '舒张压指标值',
   LSBP_res decimal(10,2) comment '左侧收缩压指标值',
   LDBP_res decimal(10,2) comment '左侧舒张压指标值',
   W_res decimal(10,2) comment '腰围指标值',
   H_res decimal(10,2) comment '臀围指标值',
   label BIGINT comment '聚类类别'
)DEFAULT CHARSET=utf8;

ALTER TABLE library_physical_check_wide_with_clustering_label_5 ADD INDEX library_physical_check_wide_with_clustering_label_5_index (user_id,check_date,time_interval);





insert into library_physical_check_wide_with_clustering_label_5 
select t_u.user_id,t_u.operate_month,t_u.time_interval,
if_gxxy,if_2tnb,if_mxzexfjb,if_gzxz,if_zqgxc,if_gnsxz,if_gzdmzyyhxxzb,if_dzpz,if_fsxgjy,if_gzss,if_ncz,
glucose_res,ALB_res,GHb_res,LDL_res,HDL_res,UCr_res,TC_res,eGFR_res,
BMI_res,SBP_res,DBP_res,LSBP_res,LDBP_res,W_res,H_res,
label from 
(select user_id,operate_month,time_interval from user_id_total_behave) t_u 
left outer join 
(select user_id,check_month,time_interval,glucose_res,ALB_res,GHb_res,LDL_res,HDL_res,UCr_res,TC_res,eGFR_res from library_check_if_normal_time_interval_wide) t_lib 
on t_u.user_id=t_lib.user_id and t_u.operate_month=t_lib.check_month and t_u.time_interval=t_lib.time_interval 
left outer join 
(select user_id,check_month,time_interval,BMI_res,SBP_res,DBP_res,LSBP_res,LDBP_res,W_res,H_res 
from physical_check_if_normal_time_interval_wide) t_phy 
on t_u.user_id=t_phy.user_id and t_u.operate_month=t_phy.check_month and t_u.time_interval=t_phy.time_interval 
left join 
(select user_id,if_gxxy,if_2tnb,if_mxzexfjb,if_gzxz,if_zqgxc,if_gnsxz,if_gzdmzyyhxxzb,if_dzpz,if_fsxgjy,if_gzss,if_ncz,label 
from disease_diagnosis_k_means_chosendiag_res_5) t_kmeans
on t_u.user_id=t_kmeans.user_id

#统计
select ifnull(label,5),
max(if_gxxy) if_gxxy,max(if_2tnb) if_2tnb,max(if_mxzexfjb) if_mxzexfjb,max(if_gzxz) if_gzxz,max(if_zqgxc) if_zqgxc,max(if_gnsxz) if_gnsxz,max(if_gzdmzyyhxxzb) if_gzdmzyyhxxzb,max(if_dzpz) if_dzpz,max(if_fsxgjy) if_fsxgjy,max(if_gzss) if_gzss,max(if_ncz) if_ncz,
avg(glucose_res) avg_glucose_res,avg(ALB_res) avg_ALB_res,avg(GHb_res) avg_GHb_res,avg(LDL_res) avg_LDL_res,avg(HDL_res) avg_HDL_res,avg(UCr_res) avg_UCr_res,avg(TC_res) avg_TC_res ,avg(eGFR_res) avg_eGFR_res,
std(glucose_res) std_glucose_res,std(ALB_res) std_ALB_res,std(GHb_res) std_GHb_res,std(LDL_res) std_LDL_res,std(HDL_res) std_HDL_res,std(UCr_res) std_UCr_res,std(TC_res) std_TC_res,std(eGFR_res) std_eGFR_res,
avg(BMI_res) avg_BMI_res,avg(SBP_res) avg_SBP_res,avg(DBP_res) avg_DBP_res,avg(LSBP_res) avg_LSBP_res,avg(LDBP_res) avg_LDBP_res,avg(W_res) avg_W_res,avg(H_res) avg_H_res,
std(BMI_res) std_BMI_res,std(SBP_res) std_SBP_res,std(DBP_res) std_DBP_res,std(LSBP_res) std_LSBP_res,std(LDBP_res) std_LDBP_res,std(W_res) std_W_res,std(H_res) std_H_res 
from library_physical_check_wide_with_clustering_label_5 group by label order by ifnull(label,5)


###基于kmeans聚类结果的建模
#1.k=3：
drop table if exists whole_data_final_with_3_label;
CREATE TABLE IF NOT EXISTS whole_data_final_with_3_label(
   user_id BIGINT comment '患者ID',
   gender VARCHAR(50) comment '性别',
   birth_date VARCHAR(50) comment '出生日期',
   age bigint comment '年龄',
   nationality VARCHAR(50) comment '民族',
   marriage VARCHAR(50) comment '婚姻状况',
   career VARCHAR(50) comment '职业',
   education VARCHAR(50) comment '学历',
   medical_insurance VARCHAR(50) comment '医疗保险',
   mechanism VARCHAR(50) comment '所在机构',
   initial_smoke_status varchar(5) DEFAULT NULL COMMENT '最初吸烟状态',
   latest_smoke_status varchar(5) DEFAULT NULL COMMENT '最近吸烟状态',
   operate_month varchar(50) DEFAULT NULL COMMENT '月份',
   time_interval varchar(50) DEFAULT NULL COMMENT '时间间隔',
   if_gxy int(11) DEFAULT NULL COMMENT '是否诊断为高血压',
   if_tnb int(11) DEFAULT NULL COMMENT '是否诊断为糖尿病',
   if_xzb int(11) DEFAULT NULL COMMENT '是否诊断为冠状动脉粥样硬化性心脏病',
   if_ncz int(11) DEFAULT NULL COMMENT '是否诊断为脑卒中',
   if_gxz int(11) DEFAULT NULL COMMENT '是否诊断为高血脂症',
   if_cancer int(11) DEFAULT NULL COMMENT '是否诊断为癌症',
   if_dnjsz int(11) DEFAULT NULL COMMENT '是否诊断为胆囊结石症',
   if_mxzexfjb int(11) DEFAULT NULL COMMENT '是否诊断为慢性阻塞性肺疾病',
   if_other int(11) DEFAULT NULL COMMENT '是否诊断为其他',
   if_wy int(11) DEFAULT NULL COMMENT '是否诊断为胃炎',
   BP_drug_type_cnt int(11) DEFAULT NULL COMMENT '抗高血压药服用药品类型数',
   TC_drug_type_cnt int(11) DEFAULT NULL COMMENT '抗高血脂药服用药品类型数',
   Glu_drug_type_cnt int(11) DEFAULT NULL COMMENT '抗高血糖药服用药品类型数',
   VTE_drug_type_cnt int(11) DEFAULT NULL COMMENT '抗血栓药服用药品类型数',
   Vitamin_drug_type_cnt int(11) DEFAULT NULL COMMENT '维生素类服用药品类型数',
   PNS_drug_type_cnt int(11) DEFAULT NULL COMMENT '周围神经保护药服用药品类型数',
   RI_drug_type_cnt int(11) DEFAULT NULL COMMENT '胰岛素针头使用药品类型数',
   DM_drug_type_cnt int(11) DEFAULT NULL COMMENT '预防糖尿病视网膜病变药服用 药品类型数',
   NR_drug_type_cnt int(11) DEFAULT NULL COMMENT '神经调节药服用药品类型数',
   BVTE_drug_type_cnt int(11) DEFAULT NULL COMMENT '抗血血栓药服用药品类型数',
   other_drug_type_cnt int(11) DEFAULT NULL COMMENT '其他药使用药品类型数',
   BP_drug_using_cnt int(11) DEFAULT NULL COMMENT '抗高血压药服用次数',
   TC_drug_using_cnt int(11) DEFAULT NULL COMMENT '抗高血脂药服用次数',
   Glu_drug_using_cnt int(11) DEFAULT NULL COMMENT '抗高血糖药服用次数',
   VTE_drug_using_cnt int(11) DEFAULT NULL COMMENT '抗血栓药服用次数',
   Vitamin_drug_using_cnt int(11) DEFAULT NULL COMMENT '维生素类服用次数',
   PNS_drug_using_cnt int(11) DEFAULT NULL COMMENT '周围神经保护药服用次数',
   RI_drug_using_cnt int(11) DEFAULT NULL COMMENT '胰岛素针头使用次数',
   DM_drug_using_cnt int(11) DEFAULT NULL COMMENT '预防糖尿病视网膜病变药服用次数',
   NR_drug_using_cnt int(11) DEFAULT NULL COMMENT '神经调节药服用次数',
   BVTE_drug_using_cnt int(11) DEFAULT NULL COMMENT '抗血血栓药服次数',
   other_drug_using_cnt int(11) DEFAULT NULL COMMENT '其他药使用次数',   
   glucose_nr decimal(10,2) DEFAULT NULL COMMENT '葡萄糖正常比例',
   ALB_nr decimal(10,2) DEFAULT NULL COMMENT '尿微量白蛋白正常比例',
   GHb_nr decimal(10,2) DEFAULT NULL COMMENT '糖化血红蛋白正常比例',
   LDL_nr decimal(10,2) DEFAULT NULL COMMENT '低密度脂蛋白胆固醇正常比例',
   HDL_nr decimal(10,2) DEFAULT NULL COMMENT '高密度脂蛋白胆固醇正常比例',
   UCr_nr decimal(10,2) DEFAULT NULL COMMENT '肌酐测定正常比例',
   TC_nr decimal(10,2) DEFAULT NULL COMMENT '总胆固醇正常比例',
   eGFR_nr decimal(10,2) DEFAULT NULL COMMENT 'eGFR正常比例',
   glucose_cnt int(11) DEFAULT NULL COMMENT '葡萄糖检查次数',
   ALB_cnt int(11) DEFAULT NULL COMMENT '尿微量白蛋白检查次数',
   GHb_cnt int(11) DEFAULT NULL COMMENT '糖化血红蛋白检查次数',
   LDL_cnt int(11) DEFAULT NULL COMMENT '低密度脂蛋白胆固醇检查次数',
   HDL_cnt int(11) DEFAULT NULL COMMENT '高密度脂蛋白胆固醇检查次数',
   UCr_cnt int(11) DEFAULT NULL COMMENT '肌酐测定检查次数',
   TC_cnt int(11) DEFAULT NULL COMMENT '总胆固醇检查次数',
   eGFR_cnt int(11) DEFAULT NULL COMMENT 'eGFR检查次数',
   glucose_res decimal(10,2) DEFAULT NULL COMMENT '葡萄糖指标值',
   ALB_res decimal(10,2) DEFAULT NULL COMMENT '尿微量白蛋白指标值',
   GHb_res decimal(10,2) DEFAULT NULL COMMENT '糖化血红蛋白指标值',
   LDL_res decimal(10,2) DEFAULT NULL COMMENT '低密度脂蛋白胆固醇指标值',
   HDL_res decimal(10,2) DEFAULT NULL COMMENT '高密度脂蛋白胆固醇指标值',
   UCr_res decimal(10,2) DEFAULT NULL COMMENT '肌酐测定指标值',
   TC_res decimal(10,2) DEFAULT NULL COMMENT '总胆固醇指标值',
   eGFR_res decimal(10,2) DEFAULT NULL COMMENT 'eGFR指标值',
   glucose_diff decimal(10,2) DEFAULT NULL COMMENT '葡萄糖变化值',
   ALB_diff decimal(10,2) DEFAULT NULL COMMENT '尿微量白蛋白变化值',
   GHb_diff decimal(10,2) DEFAULT NULL COMMENT '糖化血红蛋白变化值',
   LDL_diff decimal(10,2) DEFAULT NULL COMMENT '低密度脂蛋白胆固醇变化值',
   HDL_diff decimal(10,2) DEFAULT NULL COMMENT '高密度脂蛋白胆固醇变化值',
   UCr_diff decimal(10,2) DEFAULT NULL COMMENT '肌酐测定变化值',
   TC_diff decimal(10,2) DEFAULT NULL COMMENT '总胆固醇变化值',
   eGFR_diff decimal(10,2) DEFAULT NULL COMMENT 'eGFR变化值',   
   BMI_nr decimal(10,2) DEFAULT NULL COMMENT 'BMI正常比例',
   SBP_nr decimal(10,2) DEFAULT NULL COMMENT '收缩压正常比例',
   DBP_nr decimal(10,2) DEFAULT NULL COMMENT '舒张压正常比例',
   W_nr decimal(10,2) DEFAULT NULL COMMENT '腰围正常比例',
   BMI_cnt int(11) DEFAULT NULL COMMENT 'BMI检查次数',
   SBP_cnt int(11) DEFAULT NULL COMMENT '收缩压检查次数',
   DBP_cnt int(11) DEFAULT NULL COMMENT '舒张压检查次数',
   LSBP_cnt int(11) DEFAULT NULL COMMENT '左侧收缩压检查次数',
   LDBP_cnt int(11) DEFAULT NULL COMMENT '左侧舒张压检查次数',
   W_cnt int(11) DEFAULT NULL COMMENT '腰围检查次数',
   H_cnt int(11) DEFAULT NULL COMMENT '臀围检查次数',
   BMI_res decimal(10,2) DEFAULT NULL COMMENT 'BMI指标值',
   SBP_res decimal(10,2) DEFAULT NULL COMMENT '收缩压指标值',
   DBP_res decimal(10,2) DEFAULT NULL COMMENT '舒张压指标值',
   LSBP_res decimal(10,2) DEFAULT NULL COMMENT '左侧收缩压指标值',
   LDBP_res decimal(10,2) DEFAULT NULL COMMENT '左侧舒张压指标值',
   W_res decimal(10,2) DEFAULT NULL COMMENT '腰围指标值',
   H_res decimal(10,2) DEFAULT NULL COMMENT '臀围指标值',
   BMI_diff decimal(10,2) DEFAULT NULL COMMENT 'BMI指标值',
   SBP_diff decimal(10,2) DEFAULT NULL COMMENT '收缩压指标值',
   DBP_diff decimal(10,2) DEFAULT NULL COMMENT '舒张压指标值',
   LSBP_diff decimal(10,2) DEFAULT NULL COMMENT '左侧收缩压指标值',
   LDBP_diff decimal(10,2) DEFAULT NULL COMMENT '左侧舒张压指标值',
   W_diff decimal(10,2) DEFAULT NULL COMMENT '腰围指标值',
   H_diff decimal(10,2) DEFAULT NULL COMMENT '臀围指标值',
   label BIGINT comment '聚类类别'
)DEFAULT CHARSET=utf8;

ALTER TABLE whole_data_final_with_3_label ADD INDEX whole_data_final_with_3_label_index (user_id,operate_month,time_interval);



insert into whole_data_final_with_3_label 
select t_u.user_id,
t_info.gender,t_info.birth_date,t_info.age,t_info.nationality,t_info.marriage,t_info.career,t_info.education,t_info.medical_insurance,t_info.mechanism,
t_smoke.initial_smoke_status,t_smoke.latest_smoke_status,
t_u.operate_month,t_u.time_interval,
if_gxy,if_tnb,if_xzb,if_ncz,if_gxz,if_cancer,if_dnjsz,if_mxzexfjb,if_other,if_wy,
BP_drug_type_cnt,TC_drug_type_cnt,Glu_drug_type_cnt,VTE_drug_type_cnt,Vitamin_drug_type_cnt,PNS_drug_type_cnt,RI_drug_type_cnt, DM_drug_type_cnt,NR_drug_type_cnt,BVTE_drug_type_cnt,other_drug_type_cnt,BP_drug_using_cnt,TC_drug_using_cnt,Glu_drug_using_cnt,VTE_drug_using_cnt,Vitamin_drug_using_cnt,PNS_drug_using_cnt,RI_drug_using_cnt,DM_drug_using_cnt,NR_drug_using_cnt,BVTE_drug_using_cnt,other_drug_using_cnt,
glucose_nr,ALB_nr,GHb_nr,LDL_nr,HDL_nr,UCr_nr,TC_nr,eGFR_nr,glucose_cnt,ALB_cnt,GHb_cnt,LDL_cnt,HDL_cnt,UCr_cnt,TC_cnt,eGFR_cnt,glucose_res,ALB_res,GHb_res,LDL_res,HDL_res,UCr_res,TC_res,eGFR_res,glucose_diff,ALB_diff,GHb_diff,LDL_diff,HDL_diff,UCr_diff,TC_diff,eGFR_diff,
BMI_nr,SBP_nr,DBP_nr,W_nr,BMI_cnt,SBP_cnt,DBP_cnt,LSBP_cnt,LDBP_cnt,W_cnt,H_cnt,BMI_res,SBP_res,DBP_res,LSBP_res,LDBP_res,W_res,H_res,BMI_diff,SBP_diff,DBP_diff,LSBP_diff,LDBP_diff,W_diff,H_diff,
ifnull(label,3) label from 
(select user_id,operate_month,time_interval from user_id_total_behave) t_u 
left outer join 
(select user_id,gender,birth_date,TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) age,nationality,marriage,career,education ,medical_insurance,mechanism from user_info 
where length(birth_date)>1) t_info
on t_u.user_id=t_info.user_id 
left outer join 
(select user_id,diagnosis_month,time_interval,if_gxy,if_tnb,if_xzb,if_ncz,if_gxz,if_cancer,if_dnjsz,if_mxzexfjb,if_other,if_wy from disease_diagnosis_time_interval_final_wide) t_dis 
on t_u.user_id=t_dis.user_id and t_u.operate_month=t_dis.diagnosis_month and t_u.time_interval=t_dis.time_interval 
left outer join 
(select user_id,using_month,time_interval,BP_drug_type_cnt,TC_drug_type_cnt,Glu_drug_type_cnt,VTE_drug_type_cnt,Vitamin_drug_type_cnt,PNS_drug_type_cnt,RI_drug_type_cnt, DM_drug_type_cnt,NR_drug_type_cnt,BVTE_drug_type_cnt,other_drug_type_cnt,BP_drug_using_cnt,TC_drug_using_cnt,Glu_drug_using_cnt,VTE_drug_using_cnt,Vitamin_drug_using_cnt,PNS_drug_using_cnt,RI_drug_using_cnt,DM_drug_using_cnt,NR_drug_using_cnt,BVTE_drug_using_cnt,other_drug_using_cnt from drug_using_count_res_time_interval_wide) t_drug 
on t_u.user_id=t_drug.user_id and t_u.operate_month=t_drug.using_month and t_u.time_interval=t_drug.time_interval 
left outer join 
(select user_id,check_month,time_interval,glucose_nr,ALB_nr,GHb_nr,LDL_nr,HDL_nr,UCr_nr,TC_nr,eGFR_nr,glucose_cnt,ALB_cnt,GHb_cnt,LDL_cnt,HDL_cnt,UCr_cnt,TC_cnt,eGFR_cnt,glucose_res,ALB_res,GHb_res,LDL_res,HDL_res,UCr_res,TC_res,eGFR_res,glucose_diff,ALB_diff,GHb_diff,LDL_diff,HDL_diff,UCr_diff,TC_diff,eGFR_diff from library_check_if_normal_time_interval_wide) t_lib 
on t_u.user_id=t_lib.user_id and t_u.operate_month=t_lib.check_month and t_u.time_interval=t_lib.time_interval 
left outer join 
(select user_id,check_month,time_interval,BMI_nr,SBP_nr,DBP_nr,W_nr,BMI_cnt,SBP_cnt,DBP_cnt,LSBP_cnt,LDBP_cnt,W_cnt,H_cnt,BMI_res,SBP_res,DBP_res,LSBP_res,LDBP_res,W_res,H_res,BMI_diff,SBP_diff,DBP_diff,LSBP_diff,LDBP_diff,W_diff,H_diff 
from physical_check_if_normal_time_interval_wide) t_phy 
on t_u.user_id=t_phy.user_id and t_u.operate_month=t_phy.check_month and t_u.time_interval=t_phy.time_interval 
left outer join 
(select user_id,initial_smoke_status,latest_smoke_status from smoke_issue_final) t_smoke
on t_u.user_id=t_smoke.user_id
left outer join 
(select user_id,label from disease_diagnosis_k_means_chosendiag_res_3) t_label 
on t_u.user_id=t_label.user_id



#2.k=5：
drop table if exists whole_data_final_with_5_label;
CREATE TABLE IF NOT EXISTS whole_data_final_with_5_label(
   user_id BIGINT comment '患者ID',
   gender VARCHAR(50) comment '性别',
   birth_date VARCHAR(50) comment '出生日期',
   age bigint comment '年龄',
   nationality VARCHAR(50) comment '民族',
   marriage VARCHAR(50) comment '婚姻状况',
   career VARCHAR(50) comment '职业',
   education VARCHAR(50) comment '学历',
   medical_insurance VARCHAR(50) comment '医疗保险',
   mechanism VARCHAR(50) comment '所在机构',
   initial_smoke_status varchar(5) DEFAULT NULL COMMENT '最初吸烟状态',
   latest_smoke_status varchar(5) DEFAULT NULL COMMENT '最近吸烟状态',
   operate_month varchar(50) DEFAULT NULL COMMENT '月份',
   time_interval varchar(50) DEFAULT NULL COMMENT '时间间隔',
   if_gxy int(11) DEFAULT NULL COMMENT '是否诊断为高血压',
   if_tnb int(11) DEFAULT NULL COMMENT '是否诊断为糖尿病',
   if_xzb int(11) DEFAULT NULL COMMENT '是否诊断为冠状动脉粥样硬化性心脏病',
   if_ncz int(11) DEFAULT NULL COMMENT '是否诊断为脑卒中',
   if_gxz int(11) DEFAULT NULL COMMENT '是否诊断为高血脂症',
   if_cancer int(11) DEFAULT NULL COMMENT '是否诊断为癌症',
   if_dnjsz int(11) DEFAULT NULL COMMENT '是否诊断为胆囊结石症',
   if_mxzexfjb int(11) DEFAULT NULL COMMENT '是否诊断为慢性阻塞性肺疾病',
   if_other int(11) DEFAULT NULL COMMENT '是否诊断为其他',
   if_wy int(11) DEFAULT NULL COMMENT '是否诊断为胃炎',
   BP_drug_type_cnt int(11) DEFAULT NULL COMMENT '抗高血压药服用药品类型数',
   TC_drug_type_cnt int(11) DEFAULT NULL COMMENT '抗高血脂药服用药品类型数',
   Glu_drug_type_cnt int(11) DEFAULT NULL COMMENT '抗高血糖药服用药品类型数',
   VTE_drug_type_cnt int(11) DEFAULT NULL COMMENT '抗血栓药服用药品类型数',
   Vitamin_drug_type_cnt int(11) DEFAULT NULL COMMENT '维生素类服用药品类型数',
   PNS_drug_type_cnt int(11) DEFAULT NULL COMMENT '周围神经保护药服用药品类型数',
   RI_drug_type_cnt int(11) DEFAULT NULL COMMENT '胰岛素针头使用药品类型数',
   DM_drug_type_cnt int(11) DEFAULT NULL COMMENT '预防糖尿病视网膜病变药服用 药品类型数',
   NR_drug_type_cnt int(11) DEFAULT NULL COMMENT '神经调节药服用药品类型数',
   BVTE_drug_type_cnt int(11) DEFAULT NULL COMMENT '抗血血栓药服用药品类型数',
   other_drug_type_cnt int(11) DEFAULT NULL COMMENT '其他药使用药品类型数',
   BP_drug_using_cnt int(11) DEFAULT NULL COMMENT '抗高血压药服用次数',
   TC_drug_using_cnt int(11) DEFAULT NULL COMMENT '抗高血脂药服用次数',
   Glu_drug_using_cnt int(11) DEFAULT NULL COMMENT '抗高血糖药服用次数',
   VTE_drug_using_cnt int(11) DEFAULT NULL COMMENT '抗血栓药服用次数',
   Vitamin_drug_using_cnt int(11) DEFAULT NULL COMMENT '维生素类服用次数',
   PNS_drug_using_cnt int(11) DEFAULT NULL COMMENT '周围神经保护药服用次数',
   RI_drug_using_cnt int(11) DEFAULT NULL COMMENT '胰岛素针头使用次数',
   DM_drug_using_cnt int(11) DEFAULT NULL COMMENT '预防糖尿病视网膜病变药服用次数',
   NR_drug_using_cnt int(11) DEFAULT NULL COMMENT '神经调节药服用次数',
   BVTE_drug_using_cnt int(11) DEFAULT NULL COMMENT '抗血血栓药服次数',
   other_drug_using_cnt int(11) DEFAULT NULL COMMENT '其他药使用次数',   
   glucose_nr decimal(10,2) DEFAULT NULL COMMENT '葡萄糖正常比例',
   ALB_nr decimal(10,2) DEFAULT NULL COMMENT '尿微量白蛋白正常比例',
   GHb_nr decimal(10,2) DEFAULT NULL COMMENT '糖化血红蛋白正常比例',
   LDL_nr decimal(10,2) DEFAULT NULL COMMENT '低密度脂蛋白胆固醇正常比例',
   HDL_nr decimal(10,2) DEFAULT NULL COMMENT '高密度脂蛋白胆固醇正常比例',
   UCr_nr decimal(10,2) DEFAULT NULL COMMENT '肌酐测定正常比例',
   TC_nr decimal(10,2) DEFAULT NULL COMMENT '总胆固醇正常比例',
   eGFR_nr decimal(10,2) DEFAULT NULL COMMENT 'eGFR正常比例',
   glucose_cnt int(11) DEFAULT NULL COMMENT '葡萄糖检查次数',
   ALB_cnt int(11) DEFAULT NULL COMMENT '尿微量白蛋白检查次数',
   GHb_cnt int(11) DEFAULT NULL COMMENT '糖化血红蛋白检查次数',
   LDL_cnt int(11) DEFAULT NULL COMMENT '低密度脂蛋白胆固醇检查次数',
   HDL_cnt int(11) DEFAULT NULL COMMENT '高密度脂蛋白胆固醇检查次数',
   UCr_cnt int(11) DEFAULT NULL COMMENT '肌酐测定检查次数',
   TC_cnt int(11) DEFAULT NULL COMMENT '总胆固醇检查次数',
   eGFR_cnt int(11) DEFAULT NULL COMMENT 'eGFR检查次数',
   glucose_res decimal(10,2) DEFAULT NULL COMMENT '葡萄糖指标值',
   ALB_res decimal(10,2) DEFAULT NULL COMMENT '尿微量白蛋白指标值',
   GHb_res decimal(10,2) DEFAULT NULL COMMENT '糖化血红蛋白指标值',
   LDL_res decimal(10,2) DEFAULT NULL COMMENT '低密度脂蛋白胆固醇指标值',
   HDL_res decimal(10,2) DEFAULT NULL COMMENT '高密度脂蛋白胆固醇指标值',
   UCr_res decimal(10,2) DEFAULT NULL COMMENT '肌酐测定指标值',
   TC_res decimal(10,2) DEFAULT NULL COMMENT '总胆固醇指标值',
   eGFR_res decimal(10,2) DEFAULT NULL COMMENT 'eGFR指标值',
   glucose_diff decimal(10,2) DEFAULT NULL COMMENT '葡萄糖变化值',
   ALB_diff decimal(10,2) DEFAULT NULL COMMENT '尿微量白蛋白变化值',
   GHb_diff decimal(10,2) DEFAULT NULL COMMENT '糖化血红蛋白变化值',
   LDL_diff decimal(10,2) DEFAULT NULL COMMENT '低密度脂蛋白胆固醇变化值',
   HDL_diff decimal(10,2) DEFAULT NULL COMMENT '高密度脂蛋白胆固醇变化值',
   UCr_diff decimal(10,2) DEFAULT NULL COMMENT '肌酐测定变化值',
   TC_diff decimal(10,2) DEFAULT NULL COMMENT '总胆固醇变化值',
   eGFR_diff decimal(10,2) DEFAULT NULL COMMENT 'eGFR变化值',   
   BMI_nr decimal(10,2) DEFAULT NULL COMMENT 'BMI正常比例',
   SBP_nr decimal(10,2) DEFAULT NULL COMMENT '收缩压正常比例',
   DBP_nr decimal(10,2) DEFAULT NULL COMMENT '舒张压正常比例',
   W_nr decimal(10,2) DEFAULT NULL COMMENT '腰围正常比例',
   BMI_cnt int(11) DEFAULT NULL COMMENT 'BMI检查次数',
   SBP_cnt int(11) DEFAULT NULL COMMENT '收缩压检查次数',
   DBP_cnt int(11) DEFAULT NULL COMMENT '舒张压检查次数',
   LSBP_cnt int(11) DEFAULT NULL COMMENT '左侧收缩压检查次数',
   LDBP_cnt int(11) DEFAULT NULL COMMENT '左侧舒张压检查次数',
   W_cnt int(11) DEFAULT NULL COMMENT '腰围检查次数',
   H_cnt int(11) DEFAULT NULL COMMENT '臀围检查次数',
   BMI_res decimal(10,2) DEFAULT NULL COMMENT 'BMI指标值',
   SBP_res decimal(10,2) DEFAULT NULL COMMENT '收缩压指标值',
   DBP_res decimal(10,2) DEFAULT NULL COMMENT '舒张压指标值',
   LSBP_res decimal(10,2) DEFAULT NULL COMMENT '左侧收缩压指标值',
   LDBP_res decimal(10,2) DEFAULT NULL COMMENT '左侧舒张压指标值',
   W_res decimal(10,2) DEFAULT NULL COMMENT '腰围指标值',
   H_res decimal(10,2) DEFAULT NULL COMMENT '臀围指标值',
   BMI_diff decimal(10,2) DEFAULT NULL COMMENT 'BMI指标值',
   SBP_diff decimal(10,2) DEFAULT NULL COMMENT '收缩压指标值',
   DBP_diff decimal(10,2) DEFAULT NULL COMMENT '舒张压指标值',
   LSBP_diff decimal(10,2) DEFAULT NULL COMMENT '左侧收缩压指标值',
   LDBP_diff decimal(10,2) DEFAULT NULL COMMENT '左侧舒张压指标值',
   W_diff decimal(10,2) DEFAULT NULL COMMENT '腰围指标值',
   H_diff decimal(10,2) DEFAULT NULL COMMENT '臀围指标值',
   label BIGINT comment '聚类类别'
)DEFAULT CHARSET=utf8;

ALTER TABLE whole_data_final_with_5_label ADD INDEX whole_data_final_with_5_label_index (user_id,operate_month,time_interval);



insert into whole_data_final_with_5_label 
select t_u.user_id,
t_info.gender,t_info.birth_date,t_info.age,t_info.nationality,t_info.marriage,t_info.career,t_info.education,t_info.medical_insurance,t_info.mechanism,
t_smoke.initial_smoke_status,t_smoke.latest_smoke_status,
t_u.operate_month,t_u.time_interval,
if_gxy,if_tnb,if_xzb,if_ncz,if_gxz,if_cancer,if_dnjsz,if_mxzexfjb,if_other,if_wy,
BP_drug_type_cnt,TC_drug_type_cnt,Glu_drug_type_cnt,VTE_drug_type_cnt,Vitamin_drug_type_cnt,PNS_drug_type_cnt,RI_drug_type_cnt, DM_drug_type_cnt,NR_drug_type_cnt,BVTE_drug_type_cnt,other_drug_type_cnt,BP_drug_using_cnt,TC_drug_using_cnt,Glu_drug_using_cnt,VTE_drug_using_cnt,Vitamin_drug_using_cnt,PNS_drug_using_cnt,RI_drug_using_cnt,DM_drug_using_cnt,NR_drug_using_cnt,BVTE_drug_using_cnt,other_drug_using_cnt,
glucose_nr,ALB_nr,GHb_nr,LDL_nr,HDL_nr,UCr_nr,TC_nr,eGFR_nr,glucose_cnt,ALB_cnt,GHb_cnt,LDL_cnt,HDL_cnt,UCr_cnt,TC_cnt,eGFR_cnt,glucose_res,ALB_res,GHb_res,LDL_res,HDL_res,UCr_res,TC_res,eGFR_res,glucose_diff,ALB_diff,GHb_diff,LDL_diff,HDL_diff,UCr_diff,TC_diff,eGFR_diff,
BMI_nr,SBP_nr,DBP_nr,W_nr,BMI_cnt,SBP_cnt,DBP_cnt,LSBP_cnt,LDBP_cnt,W_cnt,H_cnt,BMI_res,SBP_res,DBP_res,LSBP_res,LDBP_res,W_res,H_res,BMI_diff,SBP_diff,DBP_diff,LSBP_diff,LDBP_diff,W_diff,H_diff,
ifnull(label,3) label from 
(select user_id,operate_month,time_interval from user_id_total_behave) t_u 
left outer join 
(select user_id,gender,birth_date,TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) age,nationality,marriage,career,education ,medical_insurance,mechanism from user_info 
where length(birth_date)>1) t_info
on t_u.user_id=t_info.user_id 
left outer join 
(select user_id,diagnosis_month,time_interval,if_gxy,if_tnb,if_xzb,if_ncz,if_gxz,if_cancer,if_dnjsz,if_mxzexfjb,if_other,if_wy from disease_diagnosis_time_interval_final_wide) t_dis 
on t_u.user_id=t_dis.user_id and t_u.operate_month=t_dis.diagnosis_month and t_u.time_interval=t_dis.time_interval 
left outer join 
(select user_id,using_month,time_interval,BP_drug_type_cnt,TC_drug_type_cnt,Glu_drug_type_cnt,VTE_drug_type_cnt,Vitamin_drug_type_cnt,PNS_drug_type_cnt,RI_drug_type_cnt, DM_drug_type_cnt,NR_drug_type_cnt,BVTE_drug_type_cnt,other_drug_type_cnt,BP_drug_using_cnt,TC_drug_using_cnt,Glu_drug_using_cnt,VTE_drug_using_cnt,Vitamin_drug_using_cnt,PNS_drug_using_cnt,RI_drug_using_cnt,DM_drug_using_cnt,NR_drug_using_cnt,BVTE_drug_using_cnt,other_drug_using_cnt from drug_using_count_res_time_interval_wide) t_drug 
on t_u.user_id=t_drug.user_id and t_u.operate_month=t_drug.using_month and t_u.time_interval=t_drug.time_interval 
left outer join 
(select user_id,check_month,time_interval,glucose_nr,ALB_nr,GHb_nr,LDL_nr,HDL_nr,UCr_nr,TC_nr,eGFR_nr,glucose_cnt,ALB_cnt,GHb_cnt,LDL_cnt,HDL_cnt,UCr_cnt,TC_cnt,eGFR_cnt,glucose_res,ALB_res,GHb_res,LDL_res,HDL_res,UCr_res,TC_res,eGFR_res,glucose_diff,ALB_diff,GHb_diff,LDL_diff,HDL_diff,UCr_diff,TC_diff,eGFR_diff from library_check_if_normal_time_interval_wide) t_lib 
on t_u.user_id=t_lib.user_id and t_u.operate_month=t_lib.check_month and t_u.time_interval=t_lib.time_interval 
left outer join 
(select user_id,check_month,time_interval,BMI_nr,SBP_nr,DBP_nr,W_nr,BMI_cnt,SBP_cnt,DBP_cnt,LSBP_cnt,LDBP_cnt,W_cnt,H_cnt,BMI_res,SBP_res,DBP_res,LSBP_res,LDBP_res,W_res,H_res,BMI_diff,SBP_diff,DBP_diff,LSBP_diff,LDBP_diff,W_diff,H_diff 
from physical_check_if_normal_time_interval_wide) t_phy 
on t_u.user_id=t_phy.user_id and t_u.operate_month=t_phy.check_month and t_u.time_interval=t_phy.time_interval 
left outer join 
(select user_id,initial_smoke_status,latest_smoke_status from smoke_issue_final) t_smoke
on t_u.user_id=t_smoke.user_id
left outer join 
(select user_id,label from disease_diagnosis_k_means_chosendiag_res_5) t_label 
on t_u.user_id=t_label.user_id




