drop table if exists disease_diagnosis_top_100_for_kmeans_wide;
CREATE TABLE IF NOT EXISTS disease_diagnosis_top_100_for_kmeans_wide(
   user_id BIGINT comment '患者ID',
   if_gxy int comment '高血压',
   if_ncz int comment '脑卒中',
   if_gzdmzyyhxxzb int comment '冠状动脉粥样硬化性心脏病',
   if_cancer int comment '癌症',
   if_ohter int comment '其他',
   if_gnsxz int comment '高尿酸血症',
   if_qlxzsz int comment '前列腺增生症',
   if_mxzsxfjb int comment '慢性阻塞性肺疾病',
   if_jlz int comment '焦虑症',
   if_lwy int comment '阑尾炎',
   if_Menieres_disease int comment '梅尼埃[美尼尔]病(Meniere‘s  disease)(前庭功能疾患 )',
   if_gxyxxzb int comment '高血压性心脏病',
   if_jzxgnjtz int comment '甲状腺功能减退症',
   if_lcnz int comment '卵巢囊肿',
   if_px int comment '贫血',
   if_jmqz int comment '静脉曲张',
   if_gzjb int comment '肝脏疾病',
   if_jhb int comment '结核病',
   if_szjb int comment '肾脏疾病',
   if_bnz int comment '白内障',
   if_jshzyhyz int comment '脊髓灰质炎后遗症(小儿麻痹后遗症)',
   if_zqgy int comment '支气管炎',
   if_ylcjzxgnkj int comment '亚临床甲状腺功能亢进',
   if_jjxjzxz int comment '结节性甲状腺肿',
   if_gyh int comment '肝硬化',
   if_aezhmb int comment '阿尔茨海默病',
   if_jzxz int comment '甲状腺肿',
   if_pjssb int comment '帕金森氏病',
   if_gzss int comment '骨质疏松',
   if_zfg int comment '脂肪肝',
   if_jzb int comment '颈椎病',
   if_dxxdgh int comment '窦性心动过缓',
   if_pjsszhz int comment '帕金森氏综合征',
   if_grxjb int comment '感染性疾病',
   if_by int comment '鼻炎',
   if_tf int comment '痛风',
   if_bdxgy int comment '病毒性肝炎',
   if_wy int comment '胃炎',
   if_wky int comment '胃溃疡',
   if_dnjs int comment '胆囊结石症',
   if_otherfdcrb int comment '其他法定传染病',
   if_zdjsb int comment '重度精神病',
   if_flxsgy int comment '反流性食管炎',
   if_ggtqxxhs int comment '股骨头缺血性坏死',
   if_fjjb int comment '肺结节病',
   if_fsxgjy int comment '风湿性关节炎',
   if_xlsc int comment '心律失常',
   if_fqz int comment '肺气肿',
   if_dx int comment '癫痫',
   if_gzzs int comment '骨质增生',
   if_dxy int comment '低血压',
   if_z int comment '痔',
   if_xrb int comment '息肉病',
   if_fyxxzb int comment '肺源性心脏病',
   if_nws int comment '脑萎缩',
   if_py int comment '皮炎',
   if_fc int comment '房颤',
   if_dzpz int comment '带状疱疹',
   if_snz int comment '肾囊肿',
   if_dxxdgs int comment '窦性心动过速',
   if_yy int comment '咽炎',
   if_hzxz int comment '高脂血症',
   if_sjs int comment '肾结石',
   if_qgy int comment '青光眼',
   if_wcx int comment '胃出血',
   if_xjt int comment '心绞痛',
   if_rxzs int comment '乳腺增生',
   if_qlxy int comment '前列腺炎',
   if_gmxpy int comment '过敏性皮炎',
   if_qx int comment '气胸',
   if_zqgxc int comment '支气管哮喘',
   if_aqgkz int comment '支气管扩张',
   if_xfgzqjh int comment '心房过早去极化',
   if_dny int comment '胆囊炎',
   if_dizzy int comment '眩晕',
   if_bxz int comment '败血症',
   if_zbzhz int comment '肾病综合征',
   if_xjqx int comment '心肌缺血',
   if_sq int comment '疝气',
   if_sysy int comment '肾盂肾炎',
   if_mxqlxy int comment '慢性前列腺炎',
   if_gnz int comment '肝囊肿',
   if_em int comment '耳鸣',
   if_rxnz int comment '乳腺囊肿',
   if_xjgs int comment '心肌梗死',
   if_smza int comment '睡眠障碍',
   if_bm int comment '便秘',
   if_xtxhblc int comment '系统性红斑狼疮',
   if_sxgzbd int comment '室性过早搏动',
   if_zqxmb int comment '周期性麻痹',
   if_gy int comment '肝炎',
   if_ejbxz int comment '二尖瓣狭窄',
   if_btty int comment '扁桃体炎',
   if_cgz int comment '肠梗阻',
   if_jsflz int comment '精神分裂症',
   if_ggtwjxhs int comment '股骨头无菌性坏死',
   if_ggjb int comment '骨关节病',
   if_sy int comment '肾炎',
   if_2xtnb int comment '2型糖尿病',
   if_zldx int comment '智力低下'
)DEFAULT CHARSET=utf8;

ALTER TABLE disease_diagnosis_top_100_for_kmeans_wide ADD INDEX disease_diagnosis_top_100_for_kmeans_wide_index (user_id);

insert into disease_diagnosis_top_100_for_kmeans_wide 
SELECT user_id
,max(IF(norm_diagnosis_desc='高血压',1,0))
,max(IF(norm_diagnosis_desc='脑卒中',1,0))
,max(IF(norm_diagnosis_desc='冠状动脉粥样硬化性心脏病',1,0))
,max(IF(norm_diagnosis_desc='癌症',1,0))
,max(IF(norm_diagnosis_desc='其他',1,0))
,max(IF(norm_diagnosis_desc='高尿酸血症',1,0))
,max(IF(norm_diagnosis_desc='前列腺增生症',1,0))
,max(IF(norm_diagnosis_desc='慢性阻塞性肺疾病',1,0))
,max(IF(norm_diagnosis_desc='焦虑症',1,0))
,max(IF(norm_diagnosis_desc='阑尾炎',1,0))
,max(IF(norm_diagnosis_desc='梅尼埃[美尼尔]病(Meniere‘s  disease)(前庭功能疾患 )',1,0))
,max(IF(norm_diagnosis_desc='高血压性心脏病',1,0))
,max(IF(norm_diagnosis_desc='甲状腺功能减退症',1,0))
,max(IF(norm_diagnosis_desc='卵巢囊肿',1,0))
,max(IF(norm_diagnosis_desc='贫血',1,0))
,max(IF(norm_diagnosis_desc='静脉曲张',1,0))
,max(IF(norm_diagnosis_desc='肝脏疾病',1,0))
,max(IF(norm_diagnosis_desc='结核病',1,0))
,max(IF(norm_diagnosis_desc='肾脏疾病',1,0))
,max(IF(norm_diagnosis_desc='白内障',1,0))
,max(IF(norm_diagnosis_desc='脊髓灰质炎后遗症(小儿麻痹后遗症)',1,0))
,max(IF(norm_diagnosis_desc='支气管炎',1,0))
,max(IF(norm_diagnosis_desc='亚临床甲状腺功能亢进',1,0))
,max(IF(norm_diagnosis_desc='结节性甲状腺肿',1,0))
,max(IF(norm_diagnosis_desc='肝硬化',1,0))
,max(IF(norm_diagnosis_desc='阿尔茨海默病',1,0))
,max(IF(norm_diagnosis_desc='甲状腺肿',1,0))
,max(IF(norm_diagnosis_desc='帕金森氏病',1,0))
,max(IF(norm_diagnosis_desc='骨质疏松',1,0))
,max(IF(norm_diagnosis_desc='脂肪肝',1,0))
,max(IF(norm_diagnosis_desc='颈椎病',1,0))
,max(IF(norm_diagnosis_desc='窦性心动过缓',1,0))
,max(IF(norm_diagnosis_desc='帕金森氏综合征',1,0))
,max(IF(norm_diagnosis_desc='感染性疾病',1,0))
,max(IF(norm_diagnosis_desc='鼻炎',1,0))
,max(IF(norm_diagnosis_desc='痛风',1,0))
,max(IF(norm_diagnosis_desc='病毒性肝炎',1,0))
,max(IF(norm_diagnosis_desc='胃炎',1,0))
,max(IF(norm_diagnosis_desc='胃溃疡',1,0))
,max(IF(norm_diagnosis_desc='胆囊结石症',1,0))
,max(IF(norm_diagnosis_desc='其他法定传染病',1,0))
,max(IF(norm_diagnosis_desc='重度精神病',1,0))
,max(IF(norm_diagnosis_desc='反流性食管炎',1,0))
,max(IF(norm_diagnosis_desc='股骨头缺血性坏死',1,0))
,max(IF(norm_diagnosis_desc='肺结节病',1,0))
,max(IF(norm_diagnosis_desc='风湿性关节炎',1,0))
,max(IF(norm_diagnosis_desc='心律失常',1,0))
,max(IF(norm_diagnosis_desc='肺气肿',1,0))
,max(IF(norm_diagnosis_desc='癫痫',1,0))
,max(IF(norm_diagnosis_desc='骨质增生',1,0))
,max(IF(norm_diagnosis_desc='低血压',1,0))
,max(IF(norm_diagnosis_desc='痔',1,0))
,max(IF(norm_diagnosis_desc='息肉病',1,0))
,max(IF(norm_diagnosis_desc='肺源性心脏病',1,0))
,max(IF(norm_diagnosis_desc='脑萎缩',1,0))
,max(IF(norm_diagnosis_desc='皮炎',1,0))
,max(IF(norm_diagnosis_desc='房颤',1,0))
,max(IF(norm_diagnosis_desc='带状疱疹',1,0))
,max(IF(norm_diagnosis_desc='肾囊肿',1,0))
,max(IF(norm_diagnosis_desc='窦性心动过速',1,0))
,max(IF(norm_diagnosis_desc='咽炎',1,0))
,max(IF(norm_diagnosis_desc='高脂血症',1,0))
,max(IF(norm_diagnosis_desc='肾结石',1,0))
,max(IF(norm_diagnosis_desc='青光眼',1,0))
,max(IF(norm_diagnosis_desc='胃出血',1,0))
,max(IF(norm_diagnosis_desc='心绞痛',1,0))
,max(IF(norm_diagnosis_desc='乳腺增生',1,0))
,max(IF(norm_diagnosis_desc='前列腺炎',1,0))
,max(IF(norm_diagnosis_desc='过敏性皮炎',1,0))
,max(IF(norm_diagnosis_desc='气胸',1,0))
,max(IF(norm_diagnosis_desc='支气管哮喘',1,0))
,max(IF(norm_diagnosis_desc='支气管扩张',1,0))
,max(IF(norm_diagnosis_desc='心房过早去极化',1,0))
,max(IF(norm_diagnosis_desc='胆囊炎',1,0))
,max(IF(norm_diagnosis_desc='眩晕',1,0))
,max(IF(norm_diagnosis_desc='败血症',1,0))
,max(IF(norm_diagnosis_desc='肾病综合征',1,0))
,max(IF(norm_diagnosis_desc='心肌缺血',1,0))
,max(IF(norm_diagnosis_desc='疝气',1,0))
,max(IF(norm_diagnosis_desc='肾盂肾炎',1,0))
,max(IF(norm_diagnosis_desc='慢性前列腺炎',1,0))
,max(IF(norm_diagnosis_desc='肝囊肿',1,0))
,max(IF(norm_diagnosis_desc='耳鸣',1,0))
,max(IF(norm_diagnosis_desc='乳腺囊肿',1,0))
,max(IF(norm_diagnosis_desc='心肌梗死',1,0))
,max(IF(norm_diagnosis_desc='睡眠障碍',1,0))
,max(IF(norm_diagnosis_desc='便秘',1,0))
,max(IF(norm_diagnosis_desc='系统性红斑狼疮',1,0))
,max(IF(norm_diagnosis_desc='室性过早搏动',1,0))
,max(IF(norm_diagnosis_desc='周期性麻痹',1,0))
,max(IF(norm_diagnosis_desc='肝炎',1,0))
,max(IF(norm_diagnosis_desc='二尖瓣狭窄',1,0))
,max(IF(norm_diagnosis_desc='扁桃体炎',1,0))
,max(IF(norm_diagnosis_desc='肠梗阻',1,0))
,max(IF(norm_diagnosis_desc='精神分裂症',1,0))
,max(IF(norm_diagnosis_desc='股骨头无菌性坏死',1,0))
,max(IF(norm_diagnosis_desc='骨关节病',1,0))
,max(IF(norm_diagnosis_desc='肾炎',1,0))
,max(IF(norm_diagnosis_desc='2型糖尿病',1,0))
,max(IF(norm_diagnosis_desc='智力低下',1,0))
FROM disease_diagnosis_final   
GROUP BY user_id 

高血压,脑卒中,冠状动脉粥样硬化性心脏病,癌症,其他,高尿酸血症,前列腺增生症,慢性阻塞性肺疾病,焦虑症,阑尾炎,梅尼埃[美尼尔]病(Meniere‘s  disease)(前庭功能疾患 ),高血压性心脏病,甲状腺功能减退症,卵巢囊肿,贫血,静脉曲张,肝脏疾病,结核病,肾脏疾病,白内障,脊髓灰质炎后遗症(小儿麻痹后遗症),支气管炎,亚临床甲状腺功能亢进,结节性甲状腺肿,肝硬化,阿尔茨海默病,甲状腺肿,帕金森氏病,骨质疏松,脂肪肝,颈椎病,窦性心动过缓,帕金森氏综合征,感染性疾病,鼻炎,痛风,病毒性肝炎,胃炎,胃溃疡,胆囊结石症,其他法定传染病,重度精神病,反流性食管炎,股骨头缺血性坏死,肺结节病,风湿性关节炎,心律失常,肺气肿,癫痫,骨质增生,低血压,痔,息肉病,肺源性心脏病,脑萎缩,皮炎,房颤,带状疱疹,肾囊肿,窦性心动过速,咽炎,高脂血症,肾结石,青光眼,胃出血,心绞痛,乳腺增生,前列腺炎,过敏性皮炎,气胸,支气管哮喘,支气管扩张,心房过早去极化,胆囊炎,眩晕,败血症,肾病综合征,心肌缺血,疝气,肾盂肾炎,慢性前列腺炎,肝囊肿,耳鸣,乳腺囊肿,心肌梗死,睡眠障碍,便秘,系统性红斑狼疮,室性过早搏动,周期性麻痹,肝炎,二尖瓣狭窄,扁桃体炎,肠梗阻,精神分裂症,股骨头无菌性坏死,骨关节病,肾炎,2型糖尿病,智力低下




drop table if exists disease_diagnosis_top_100_wide_source;
CREATE TABLE IF NOT EXISTS disease_diagnosis_top_100_wide_source(
   id int primary key auto_increment comment '自增序列' ,
   user_id BIGINT comment '患者ID',
   if_gxy int comment '高血压',
   if_ncz int comment '脑卒中',
   if_gzdmzyyhxxzb int comment '冠状动脉粥样硬化性心脏病',
   if_cancer int comment '癌症',
   if_ohter int comment '其他',
   if_gnsxz int comment '高尿酸血症',
   if_qlxzsz int comment '前列腺增生症',
   if_mxzsxfjb int comment '慢性阻塞性肺疾病',
   if_jlz int comment '焦虑症',
   if_lwy int comment '阑尾炎',
   if_Menieres_disease int comment '梅尼埃[美尼尔]病(Meniere‘s  disease)(前庭功能疾患 )',
   if_gxyxxzb int comment '高血压性心脏病',
   if_jzxgnjtz int comment '甲状腺功能减退症',
   if_lcnz int comment '卵巢囊肿',
   if_px int comment '贫血',
   if_jmqz int comment '静脉曲张',
   if_gzjb int comment '肝脏疾病',
   if_jhb int comment '结核病',
   if_szjb int comment '肾脏疾病',
   if_bnz int comment '白内障',
   if_jshzyhyz int comment '脊髓灰质炎后遗症(小儿麻痹后遗症)',
   if_zqgy int comment '支气管炎',
   if_ylcjzxgnkj int comment '亚临床甲状腺功能亢进',
   if_jjxjzxz int comment '结节性甲状腺肿',
   if_gyh int comment '肝硬化',
   if_aezhmb int comment '阿尔茨海默病',
   if_jzxz int comment '甲状腺肿',
   if_pjssb int comment '帕金森氏病',
   if_gzss int comment '骨质疏松',
   if_zfg int comment '脂肪肝',
   if_jzb int comment '颈椎病',
   if_dxxdgh int comment '窦性心动过缓',
   if_pjsszhz int comment '帕金森氏综合征',
   if_grxjb int comment '感染性疾病',
   if_by int comment '鼻炎',
   if_tf int comment '痛风',
   if_bdxgy int comment '病毒性肝炎',
   if_wy int comment '胃炎',
   if_wky int comment '胃溃疡',
   if_dnjs int comment '胆囊结石症',
   if_otherfdcrb int comment '其他法定传染病',
   if_zdjsb int comment '重度精神病',
   if_flxsgy int comment '反流性食管炎',
   if_ggtqxxhs int comment '股骨头缺血性坏死',
   if_fjjb int comment '肺结节病',
   if_fsxgjy int comment '风湿性关节炎',
   if_xlsc int comment '心律失常',
   if_fqz int comment '肺气肿',
   if_dx int comment '癫痫',
   if_gzzs int comment '骨质增生',
   if_dxy int comment '低血压',
   if_z int comment '痔',
   if_xrb int comment '息肉病',
   if_fyxxzb int comment '肺源性心脏病',
   if_nws int comment '脑萎缩',
   if_py int comment '皮炎',
   if_fc int comment '房颤',
   if_dzpz int comment '带状疱疹',
   if_snz int comment '肾囊肿',
   if_dxxdgs int comment '窦性心动过速',
   if_yy int comment '咽炎',
   if_hzxz int comment '高脂血症',
   if_sjs int comment '肾结石',
   if_qgy int comment '青光眼',
   if_wcx int comment '胃出血',
   if_xjt int comment '心绞痛',
   if_rxzs int comment '乳腺增生',
   if_qlxy int comment '前列腺炎',
   if_gmxpy int comment '过敏性皮炎',
   if_qx int comment '气胸',
   if_zqgxc int comment '支气管哮喘',
   if_aqgkz int comment '支气管扩张',
   if_xfgzqjh int comment '心房过早去极化',
   if_dny int comment '胆囊炎',
   if_dizzy int comment '眩晕',
   if_bxz int comment '败血症',
   if_zbzhz int comment '肾病综合征',
   if_xjqx int comment '心肌缺血',
   if_sq int comment '疝气',
   if_sysy int comment '肾盂肾炎',
   if_mxqlxy int comment '慢性前列腺炎',
   if_gnz int comment '肝囊肿',
   if_em int comment '耳鸣',
   if_rxnz int comment '乳腺囊肿',
   if_xjgs int comment '心肌梗死',
   if_smza int comment '睡眠障碍',
   if_bm int comment '便秘',
   if_xtxhblc int comment '系统性红斑狼疮',
   if_sxgzbd int comment '室性过早搏动',
   if_zqxmb int comment '周期性麻痹',
   if_gy int comment '肝炎',
   if_ejbxz int comment '二尖瓣狭窄',
   if_btty int comment '扁桃体炎',
   if_cgz int comment '肠梗阻',
   if_jsflz int comment '精神分裂症',
   if_ggtwjxhs int comment '股骨头无菌性坏死',
   if_ggjb int comment '骨关节病',
   if_sy int comment '肾炎',
   if_2xtnb int comment '2型糖尿病',
   if_zldx int comment '智力低下'
)DEFAULT CHARSET=utf8;

ALTER TABLE disease_diagnosis_top_100_wide_source ADD INDEX disease_diagnosis_top_100_wide_source_index (id,user_id);










insert into disease_diagnosis_top_100_wide_source  (user_id,if_gxy,if_ncz,if_gzdmzyyhxxzb,if_cancer,if_ohter,if_gnsxz,if_qlxzsz,if_mxzsxfjb,if_jlz,if_lwy,if_Menieres_disease,if_gxyxxzb,if_jzxgnjtz,if_lcnz,if_px,if_jmqz,if_gzjb,if_jhb,if_szjb,if_bnz,if_jshzyhyz,if_zqgy,if_ylcjzxgnkj,if_jjxjzxz,if_gyh,if_aezhmb,if_jzxz,if_pjssb,if_gzss,if_zfg,if_jzb,if_dxxdgh,if_pjsszhz,if_grxjb,if_by,if_tf,if_bdxgy,if_wy,if_wky,if_dnjs,if_otherfdcrb,if_zdjsb,if_flxsgy,if_ggtqxxhs,if_fjjb,if_fsxgjy,if_xlsc,if_fqz,if_dx,if_gzzs,if_dxy,if_z,if_xrb,if_fyxxzb,if_nws,if_py,if_fc,if_dzpz,if_snz,if_dxxdgs,if_yy,if_hzxz,if_sjs,if_qgy,if_wcx,if_xjt,if_rxzs,if_qlxy,if_gmxpy,if_qx,if_zqgxc,if_aqgkz,if_xfgzqjh,if_dny,if_dizzy,if_bxz,if_zbzhz,if_xjqx,if_sq,if_sysy,if_mxqlxy,if_gnz,if_em,if_rxnz,if_xjgs,if_smza,if_bm,if_xtxhblc,if_sxgzbd,if_zqxmb,if_gy,if_ejbxz,if_btty,if_cgz,if_jsflz,if_ggtwjxhs,if_ggjb,if_sy,if_2xtnb,if_zldx
) select  if_gxy,if_ncz,if_gzdmzyyhxxzb,if_cancer,if_ohter,if_gnsxz,if_qlxzsz,if_mxzsxfjb,if_jlz,if_lwy,if_Menieres_disease,if_gxyxxzb,if_jzxgnjtz,if_lcnz,if_px,if_jmqz,if_gzjb,if_jhb,if_szjb,if_bnz,if_jshzyhyz,if_zqgy,if_ylcjzxgnkj,if_jjxjzxz,if_gyh,if_aezhmb,if_jzxz,if_pjssb,if_gzss,if_zfg,if_jzb,if_dxxdgh,if_pjsszhz,if_grxjb,if_by,if_tf,if_bdxgy,if_wy,if_wky,if_dnjs,if_otherfdcrb,if_zdjsb,if_flxsgy,if_ggtqxxhs,if_fjjb,if_fsxgjy,if_xlsc,if_fqz,if_dx,if_gzzs,if_dxy,if_z,if_xrb,if_fyxxzb,if_nws,if_py,if_fc,if_dzpz,if_snz,if_dxxdgs,if_yy,if_hzxz,if_sjs,if_qgy,if_wcx,if_xjt,if_rxzs,if_qlxy,if_gmxpy,if_qx,if_zqgxc,if_aqgkz,if_xfgzqjh,if_dny,if_dizzy,if_bxz,if_zbzhz,if_xjqx,if_sq,if_sysy,if_mxqlxy,if_gnz,if_em,if_rxnz,if_xjgs,if_smza,if_bm,if_xtxhblc,if_sxgzbd,if_zqxmb,if_gy,if_ejbxz,if_btty,if_cgz,if_jsflz,if_ggtwjxhs,if_ggjb,if_sy,if_2xtnb,if_zldx
 from whole_data_final_use_no_month_on_disease where if_gxy is not null and BP_drug_type_cnt is not null and glucose_nr is not null and BMI_nr is not null


drop table if exists disease_diagnosis_k_means_res_23;
CREATE TABLE IF NOT EXISTS disease_diagnosis_k_means_res_23(
   user_id BIGINT comment '患者ID',
   if_gxy int comment '高血压',
   if_ncz int comment '脑卒中',
   if_gzdmzyyhxxzb int comment '冠状动脉粥样硬化性心脏病',
   if_cancer int comment '癌症',
   if_ohter int comment '其他',
   if_gnsxz int comment '高尿酸血症',
   if_qlxzsz int comment '前列腺增生症',
   if_mxzsxfjb int comment '慢性阻塞性肺疾病',
   if_jlz int comment '焦虑症',
   if_lwy int comment '阑尾炎',
   if_Menieres_disease int comment '梅尼埃[美尼尔]病(Meniere‘s  disease)(前庭功能疾患 )',
   if_gxyxxzb int comment '高血压性心脏病',
   if_jzxgnjtz int comment '甲状腺功能减退症',
   if_lcnz int comment '卵巢囊肿',
   if_px int comment '贫血',
   if_jmqz int comment '静脉曲张',
   if_gzjb int comment '肝脏疾病',
   if_jhb int comment '结核病',
   if_szjb int comment '肾脏疾病',
   if_bnz int comment '白内障',
   if_jshzyhyz int comment '脊髓灰质炎后遗症(小儿麻痹后遗症)',
   if_zqgy int comment '支气管炎',
   if_ylcjzxgnkj int comment '亚临床甲状腺功能亢进',
   if_jjxjzxz int comment '结节性甲状腺肿',
   if_gyh int comment '肝硬化',
   if_aezhmb int comment '阿尔茨海默病',
   if_jzxz int comment '甲状腺肿',
   if_pjssb int comment '帕金森氏病',
   if_gzss int comment '骨质疏松',
   if_zfg int comment '脂肪肝',
   if_jzb int comment '颈椎病',
   if_dxxdgh int comment '窦性心动过缓',
   if_pjsszhz int comment '帕金森氏综合征',
   if_grxjb int comment '感染性疾病',
   if_by int comment '鼻炎',
   if_tf int comment '痛风',
   if_bdxgy int comment '病毒性肝炎',
   if_wy int comment '胃炎',
   if_wky int comment '胃溃疡',
   if_dnjs int comment '胆囊结石症',
   if_otherfdcrb int comment '其他法定传染病',
   if_zdjsb int comment '重度精神病',
   if_flxsgy int comment '反流性食管炎',
   if_ggtqxxhs int comment '股骨头缺血性坏死',
   if_fjjb int comment '肺结节病',
   if_fsxgjy int comment '风湿性关节炎',
   if_xlsc int comment '心律失常',
   if_fqz int comment '肺气肿',
   if_dx int comment '癫痫',
   if_gzzs int comment '骨质增生',
   if_dxy int comment '低血压',
   if_z int comment '痔',
   if_xrb int comment '息肉病',
   if_fyxxzb int comment '肺源性心脏病',
   if_nws int comment '脑萎缩',
   if_py int comment '皮炎',
   if_fc int comment '房颤',
   if_dzpz int comment '带状疱疹',
   if_snz int comment '肾囊肿',
   if_dxxdgs int comment '窦性心动过速',
   if_yy int comment '咽炎',
   if_hzxz int comment '高脂血症',
   if_sjs int comment '肾结石',
   if_qgy int comment '青光眼',
   if_wcx int comment '胃出血',
   if_xjt int comment '心绞痛',
   if_rxzs int comment '乳腺增生',
   if_qlxy int comment '前列腺炎',
   if_gmxpy int comment '过敏性皮炎',
   if_qx int comment '气胸',
   if_zqgxc int comment '支气管哮喘',
   if_aqgkz int comment '支气管扩张',
   if_xfgzqjh int comment '心房过早去极化',
   if_dny int comment '胆囊炎',
   if_dizzy int comment '眩晕',
   if_bxz int comment '败血症',
   if_zbzhz int comment '肾病综合征',
   if_xjqx int comment '心肌缺血',
   if_sq int comment '疝气',
   if_sysy int comment '肾盂肾炎',
   if_mxqlxy int comment '慢性前列腺炎',
   if_gnz int comment '肝囊肿',
   if_em int comment '耳鸣',
   if_rxnz int comment '乳腺囊肿',
   if_xjgs int comment '心肌梗死',
   if_smza int comment '睡眠障碍',
   if_bm int comment '便秘',
   if_xtxhblc int comment '系统性红斑狼疮',
   if_sxgzbd int comment '室性过早搏动',
   if_zqxmb int comment '周期性麻痹',
   if_gy int comment '肝炎',
   if_ejbxz int comment '二尖瓣狭窄',
   if_btty int comment '扁桃体炎',
   if_cgz int comment '肠梗阻',
   if_jsflz int comment '精神分裂症',
   if_ggtwjxhs int comment '股骨头无菌性坏死',
   if_ggjb int comment '骨关节病',
   if_sy int comment '肾炎',
   if_2xtnb int comment '2型糖尿病',
   if_zldx int comment '智力低下',
   label BIGINT comment '聚类类别'
)DEFAULT CHARSET=utf8;

ALTER TABLE disease_diagnosis_k_means_res_23 ADD INDEX disease_diagnosis_k_means_res_23_index (user_id);


drop table if exists disease_diagnosis_k_23_label;
CREATE TABLE IF NOT EXISTS disease_diagnosis_k_23_label(
   id BIGINT comment 'id',
   label bigint comment '聚类类别'
)DEFAULT CHARSET=utf8;
ALTER TABLE disease_diagnosis_k_23_label ADD INDEX disease_diagnosis_k_23_label_index (id);


LOAD DATA INFILE 'E:/project_data/output/data/label_23.csv' 
INTO TABLE disease_diagnosis_k_23_label 
FIELDS TERMINATED BY ',';


insert into disease_diagnosis_k_means_res_23 select user_id,if_gxy,if_ncz,if_gzdmzyyhxxzb,if_cancer,if_ohter,if_gnsxz,if_qlxzsz,if_mxzsxfjb,if_jlz,if_lwy,if_Menieres_disease,if_gxyxxzb,if_jzxgnjtz,if_lcnz,if_px,if_jmqz,if_gzjb,if_jhb,if_szjb,if_bnz,if_jshzyhyz,if_zqgy,if_ylcjzxgnkj,if_jjxjzxz,if_gyh,if_aezhmb,if_jzxz,if_pjssb,if_gzss,if_zfg,if_jzb,if_dxxdgh,if_pjsszhz,if_grxjb,if_by,if_tf,if_bdxgy,if_wy,if_wky,if_dnjs,if_otherfdcrb,if_zdjsb,if_flxsgy,if_ggtqxxhs,if_fjjb,if_fsxgjy,if_xlsc,if_fqz,if_dx,if_gzzs,if_dxy,if_z,if_xrb,if_fyxxzb,if_nws,if_py,if_fc,if_dzpz,if_snz,if_dxxdgs,if_yy,if_hzxz,if_sjs,if_qgy,if_wcx,if_xjt,if_rxzs,if_qlxy,if_gmxpy,if_qx,if_zqgxc,if_aqgkz,if_xfgzqjh,if_dny,if_dizzy,if_bxz,if_zbzhz,if_xjqx,if_sq,if_sysy,if_mxqlxy,if_gnz,if_em,if_rxnz,if_xjgs,if_smza,if_bm,if_xtxhblc,if_sxgzbd,if_zqxmb,if_gy,if_ejbxz,if_btty,if_cgz,if_jsflz,if_ggtwjxhs,if_ggjb,if_sy,if_2xtnb,if_zldx,label 
from disease_diagnosis_top_100_wide_source l 
left outer join disease_diagnosis_k_23_label r 
on l.id=r.id






