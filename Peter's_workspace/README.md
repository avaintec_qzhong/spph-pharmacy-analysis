# README #

### What is this repository for? ###

省医院药学部数据挖掘项目

### what are the folds for ###

data_pre_processing: 数据探索、处理、统计部分

mining_program：数据挖掘部分


### The function of python footages ###
data_pre_processing下：
1.python_version（python处理的部分）：
union_all_tables.py: 合并所有分表的数据

2.sql_version（sql处理的部分）：
create_table.txt 建表
export_data.txt 数据导出
k_means_1_20191031.txt 准备kmeans分析需要的数据
k_means_2_20191104_using_disease_diag_only.txt 准备kmeans分析（只用疾病诊断的信息）需要的数据
k_means_3_20191111_using_disease_diag_only_for_systemname_and_diagname.txt 准备kmeans分析（只用疾病诊断的信息）需要的数据
feature_programming.txt 部分特征工程
logistic_regression_using_kmeans_label.txt 准备（使用kmeans聚合的类别作为ground truth训练逻辑回归模型）需要的数据
null_count.txt null值分布统计
PIVOT.txt 行转列汇总
union_all_tables.txt 关联所有表形成特征表


mining_program下：
kmeans_clustering_research_using_diagd_chosen_diag.py: 使用筛选的特定诊断疾病类型进行聚类
kmeans_clustering_research_using_diagd_diagname_top100.py: 使用top100诊断疾病类型进行聚类
kmeans_clustering_research_using_diagd_system_name.py: 使用全部诊断疾病类型进行聚类（仅用名称）
kmeans_clustering_research_using_whole_data.py: 使用全部诊断疾病类型进行聚类（用全部特征）
kmeans_result_plot.py: kmeans结果可视化
training_using_kmeans_label_3.py: 训练用kmeans结果作为label的模型
training_using_stroke_label.py: 训练用脑卒中相关的疾病作为label的模型

### How do I get set up? ###
训练用kmeans结果作为label的模型：python training_using_kmeans_label_3.py
训练用脑卒中相关的疾病作为label的模型：python training_using_stroke_label.py




