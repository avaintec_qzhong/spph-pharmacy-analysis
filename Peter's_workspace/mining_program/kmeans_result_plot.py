#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt  
from pylab import mpl
mpl.rcParams['font.sans-serif'] = ['FangSong']
mpl.rcParams['axes.unicode_minus'] = False


mu1, sigma1 = 100, 15
mu2, sigma2 = 90, 20
mu3, sigma3 = 110, 10
x1 = mu1 + sigma1 * np.random.randn(10000)
x2 = mu2 + sigma2 * np.random.randn(10000)
x3 = mu3 + sigma3 * np.random.randn(10000)

k_3_gxy_x=[1,2,3]
k_3_gxy_y=[0,41758,16587]
k_3_tnb_y=[11035,0,16587]
k_3_mxzsxfb_y=[564,308,100]

#k_3_gxy_x=[1,2,3,4]
k_3_ptt_y=[5.637559,4.539365,5.456833,4.484526]
k_3_wndb_y=[3.449221,11.06105,6.552849,2.071581]
k_3_dmdzdb_y=[0.496208,2.11874,0.431866,2.206362]
k_3_gmdzdb_y=[0.288066,1.182265,0.250015,1.226332]
k_3_jg_y=[12.11209,56.403596,13.322724,52.906122]
k_3_zdgc_y=[0.973931,4.164409,0.85851,4.324436]
k_3_egfr_y=[21.276574,81.181409,18.62394,86.171749]




k_3_gxy_x=[1,2,3,4]
k_3_ptt_y=[5.637559,4.539365,5.456833,4.484526]
k_3_wndb_y=[3.449221,11.06105,6.552849,2.071581]
k_3_dmdzdb_y=[0.496208,2.11874,0.431866,2.206362]
k_3_gmdzdb_y=[0.288066,1.182265,0.250015,1.226332]
k_3_jg_y=[12.11209,56.403596,13.322724,52.906122]
k_3_zdgc_y=[0.973931,4.164409,0.85851,4.324436]
k_3_egfr_y=[21.276574,81.181409,18.62394,86.171749]

#总人数
#l1=plt.plot(k_3_gxy_x,k_3_gxy_y,'r--',label='高血压')
#l2=plt.plot(k_3_gxy_x,k_3_tnb_y,'g--',label='2型糖尿病')
#l3=plt.plot(k_3_gxy_x,k_3_mxzsxfb_y,'b--',label='慢性阻塞性肺疾病')
#plt.plot(k_3_gxy_x,k_3_gxy_y,'ro-',k_3_gxy_x,k_3_tnb_y,'g+-',k_3_gxy_x,k_3_mxzsxfb_y,'b^-')


fig = plt.figure()
ax = fig.add_subplot(1,1,1)

ax.hist([k_3_gxy_y, k_3_tnb_y, k_3_mxzsxfb_y], bins=3, normed=True, color=['red', 'green', 'blue' ], label=['高血压', '2型糖尿病', '慢性阻塞性肺疾病'],bottom=k_3_gxy_x, histtype='bar', stacked=True)

ax.set_title('kmeans聚类类别下各病诊断出总人数分布')
ax.set_xlabel('类别编号')
ax.set_ylabel('总人数')
ax.legend()
fig.savefig('C:/Users/86158/Desktop/project3/kmeans_hist_3.jpg', dpi=100)
fig.show()


##指标均值
#l1=plt.plot(k_3_gxy_x,k_3_ptt_y,'r--',label='葡萄糖')
#l2=plt.plot(k_3_gxy_x,k_3_wndb_y,'g--',label='尿微量白蛋白')
#l3=plt.plot(k_3_gxy_x,k_3_dmdzdb_y,'b--',label='低密度脂蛋白胆固醇')
#l4=plt.plot(k_3_gxy_x,k_3_gmdzdb_y,'y--',label='高密度脂蛋白胆固醇')
#l5=plt.plot(k_3_gxy_x,k_3_jg_y,'c--',label='肌酐测定')
#l6=plt.plot(k_3_gxy_x,k_3_zdgc_y,'m--',label='总胆固醇')
#l7=plt.plot(k_3_gxy_x,k_3_egfr_y,'k--',label='eGFR')

#plt.plot(k_3_gxy_x,k_3_ptt_y,'ro-',k_3_gxy_x,k_3_wndb_y,'g+-',k_3_gxy_x,k_3_dmdzdb_y,'b^-'
         #,k_3_gxy_x,k_3_gmdzdb_y,'y^-',k_3_gxy_x,k_3_jg_y,'c^-',k_3_gxy_x,k_3_zdgc_y,'m^-'
         #,k_3_gxy_x,k_3_egfr_y,'k^-')
#plt.title('kmeans聚类类别下各实验室检查指标均值分布')
#plt.xlabel('类别编号')
#plt.ylabel('指标均值')
#plt.legend()
#plt.show()