#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.externals import joblib
#from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
#import seaborn as sns
from sklearn.metrics import silhouette_score
import matplotlib.pyplot as plt


#显示所有列
#pd.set_option('display.max_columns', None)
#显示所有行
#pd.set_option('display.max_rows', None)
#设置value的显示长度为100，默认为50
pd.set_option('max_colwidth',100)

# data direct
data_file='data/k_means_source_system_name.csv'
kmeans_model_path='models/kmeans_model_diag/'

pharmacy_df_shuffle=pd.read_csv(data_file)
pharmacy_df_noshuffle=pd.read_csv(data_file)

#shuffle
pharmacy_df_shuffle=pharmacy_df_shuffle.sample(frac=1)

diag_df_shuffle=pharmacy_df_shuffle.iloc[:,1:]
diag_df_noshuffle=pharmacy_df_noshuffle.iloc[:,1:]




#使用手肘法评估最佳k值
#SSE = []
#X = np.arange(1,13)
#for k in X:
    #estimator = KMeans(n_clusters=k)  # 构造聚类器
    #estimator.fit(diag_df_shuffle)
    #SSE.append(estimator.inertia_)

#plt.xlabel('k')
#plt.ylabel('SSE')
#plt.plot(X,SSE,'o-')
#plt.show()


# 使用轮廓系数euclidean评估最佳k值
#Scores = []  
#X = [4,8,13,17,23]
#for k in X:
    #estimator = KMeans(n_clusters=k)  # 构造聚类器
    #estimator.fit(diag_df_shuffle)
    #Scores.append(silhouette_score(diag_df_shuffle,estimator.labels_,metric='euclidean',sample_size=40000,random_state=0))

#plt.xlabel('k')
#plt.ylabel('轮廓系数')
#plt.plot(X,Scores,'o-')
#plt.show()


#利用调参选择的优质k值来训练k-means模型
clf = KMeans(n_clusters=4)

s = clf.fit(diag_df_shuffle)
print (s)
 
#中心簇
print (clf.cluster_centers_)

#每个样本所属的簇
print (clf.labels_)

#用来评估簇的个数是否合适，距离越小说明簇分的越好，选取临界点的簇个数
print (clf.inertia_)

#进行预测
print (clf.predict(diag_df_shuffle))

#保存模型
joblib.dump(clf , kmeans_model_path+'km_4_c_using_diag_systemname.model')

#载入保存的模型
clf = joblib.load(kmeans_model_path+'km_4_c_using_diag_systemname.model')

#预测
res=clf.predict(diag_df_noshuffle)
res_series=pd.DataFrame(res)
i=1
#将预测结果写入文件
with open('kmeans_results_diag/using_systemname/systemname_label_4.csv','w') as f:
    for line in res:
        f.write(str(i)+','+str(line)+'\n')
        i+=1
        #print(line)

print()