#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegressionCV
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import roc_curve, auc
#import matplotlib as mpl

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from xgboost.sklearn import XGBClassifier
#from sklearn.linear_model import Lasso

from sklearn.externals import joblib
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt
from sklearn.model_selection import RandomizedSearchCV
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
#from sklearn.grid_search import GridSearchCV
#from scipy.stats import uniform
from sklearn.svm import SVC
from run_neural_networks.feature_engineering import read_csv_file_as_df,convert_category_to_num,null_filling,feature_shuffle_and_split,training_testing_split,standard_scaler,pretty_print_linear,plot_roc
k_3_source='data/k_3_label_with_full_physical_and_library_res.csv'

pharmacy_train_x_dir='data/pharmacy_train_x.csv'
pharmacy_train_y_dir='data/pharmacy_train_y.csv'
pharmacy_test_x_dir='data/pharmacy_test_x.csv'
pharmacy_test_y_dir='data/pharmacy_test_y.csv'

pharmacy_train_dir='data/pharmacy_train.csv'
pharmacy_test_dir='data/pharmacy_test.csv'

logistic_model='models/predict_using_kmeans_label/pharmacy_logistict_regression_kmeans_label_3classes.model'
logistic_model_adjust='models/predict_using_kmeans_label/pharmacy_logistict_regression_kmeans_label_3classes_adjust.model'
svm_model_adjust='models/predict_using_kmeans_label/pharmacy_svm_kmeans_label_3classes_adjust.model'
dt_model_adjust='models/predict_using_kmeans_label/pharmacy_dt_label_3classes_adjust.model'
rf_model_adjust='models/predict_using_kmeans_label/pharmacy_rf_label_3classes_adjust.model'
xgboost_model_adjust='models/predict_using_kmeans_label/pharmacy_xgboost_label_3classes_adjust.model'
xgboost_model_20200318='models/predict_using_kmeans_label/pharmacy_xgboost_label_3classes_20200318.model'

best_param_record='models/predict_using_kmeans_label/best_param_record.txt'

def write_best_param(file_path,model_name,param_dict):
    with open(file_path,'a') as writer:
        writer.write(model_name+':\n')
        for key in param_dict.keys():
            writer.write(key+':'+str(param_dict[key])+'\n')
        writer.write(model_name+' finish'+'\n'+'\n')    


pharmacy_df=pd.read_csv(k_3_source)

#显示所有列
#pd.set_option('display.max_columns', None)
#显示所有行
#pd.set_option('display.max_rows', None)

#返回每行缺失值的总数
#pharmacy_df.isnull().sum(axis=1)
#返回每列缺失值的总数
#pharmacy_df.isnull().sum(axis=0)

#存储或加载模型
def save_model(model,model_path):
    joblib.dump(model, model_path)    
    return 

def load_model(model_path):
    return joblib.load(model_path)



career=pharmacy_df['career']
mechanism=pharmacy_df['mechanism']

career_feature={career_cate:list(career.drop_duplicates().dropna()).index(career_cate) for career_cate in list(career.drop_duplicates().dropna())}
mechanism_feature={mechanism_cate:list(mechanism.drop_duplicates().dropna()).index(mechanism_cate) for mechanism_cate in list(mechanism.drop_duplicates().dropna())}
pharmacy_df['career']=pharmacy_df['career'].map(career_feature)
pharmacy_df['mechanism']=pharmacy_df['mechanism'].map(mechanism_feature)

'''特征工程'''
#缺失值填充
for column in list(pharmacy_df.columns[pharmacy_df.isnull().sum() > 0]):
    unk_value=999
    pharmacy_df[column].fillna(unk_value, inplace=True)


#特征shuffle
pharmacy_df.sample(frac=1)

y=pharmacy_df['label']
pharmacy_df.drop(['label'],axis=1,inplace=True)
X=pharmacy_df


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30, random_state=0)
#X_train.to_csv(pharmacy_train_x_dir,index=0)
#X_test.to_csv(pharmacy_test_x_dir,index=0)
#y_train.to_csv(pharmacy_train_y_dir,index=0)
#y_test.to_csv(pharmacy_test_y_dir,index=0)

sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)

##feature selection
## We use the base estimator LassoCV since the L1 norm promotes sparsity of features.
#select_model_rf = RandomForestClassifier()
##select_model_rf.fit(X_train, y_train)
#select_model_rf.fit(X_train_std, y_train)
#importances=select_model_rf.feature_importances_
#indices = np.argsort(importances)[::-1]
#print(importances)

x_columns = np.array(['性别','年龄','民族','婚姻状况','职业','学历','医疗保险','所在机构','最初吸烟状态','最近吸烟状态','时间间隔(按月份计算)','抗高血压药服用类型数'
             ,'抗高血脂药服用类型数','抗高血糖药服用类型数','抗血栓药服用类型数','维生素类服用类型数','周围神经保护药服用类型数'
             ,'胰岛素针头使用类型数','预防糖尿病视网膜病变药服用类型数','神经调节药服用类型数','抗血血栓药服用类型数','其他药使用类型数'
             ,'抗高血压药服用次数','抗高血脂药服用次数','抗高血糖药服用次数','抗血栓药服用次数','维生素类服用次数','周围神经保护药服用次数'
             ,'胰岛素针头使用次数','预防糖尿病视网膜病变药服用次数','神经调节药服用次数','抗血血栓药服次数','其他药使用次数','葡萄糖正常比例'
             ,'尿微量白蛋白正常比例','糖化血红蛋白正常比例','低密度脂蛋白胆固醇正常比例','高密度脂蛋白胆固醇正常比例','肌酐测定正常比例','总胆固醇正常比例'
             ,'eGFR正常比例','葡萄糖检查次数','尿微量白蛋白检查次数','糖化血红蛋白检查次数','低密度脂蛋白胆固醇检查次数','高密度脂蛋白胆固醇检查次数'
             ,'肌酐测定检查次数','总胆固醇检查次数','eGFR检查次数','葡萄糖指标值','尿微量白蛋白指标值','糖化血红蛋白指标值','低密度脂蛋白胆固醇指标值'
             ,'高密度脂蛋白胆固醇指标值','肌酐测定指标值','总胆固醇指标值','eGFR指标值','葡萄糖变化值','尿微量白蛋白变化值','糖化血红蛋白变化值'
             ,'低密度脂蛋白胆固醇变化值','高密度脂蛋白胆固醇变化值','肌酐测定变化值','总胆固醇变化值','eGFR变化值','BMI正常比例','收缩压正常比例'
             ,'舒张压正常比例','腰围正常比例','BMI检查次数','收缩压检查次数','舒张压检查次数','左侧收缩压检查次数','左侧舒张压检查次数','腰围检查次数'
             ,'臀围检查次数','BMI指标值','收缩压指标值','舒张压指标值','左侧收缩压指标值','左侧舒张压指标值','腰围指标值','臀围指标值','BMI变化值'
             ,'收缩压变化值','舒张压变化值','左侧收缩压变化值','左侧舒张压变化值','腰围变化值','臀围变化值'])

#画特征重要性排序

#plt.figure(figsize=(10,6))
#plt.title("特征重要性排序",fontsize = 18)
#plt.ylabel("import level",fontsize = 15,rotation=90)
#plt.rcParams['font.sans-serif'] = ["SimHei"]
#plt.rcParams['axes.unicode_minus'] = False
#for i in range(20):
    #plt.bar(i,importances[indices[i]],color='orange',align='center')
    #plt.xticks(np.arange(20),x_columns,rotation=30,fontsize=8)
#plt.show()


#extra tree
#select_model_etree = ExtraTreesClassifier()
#select_model_etree.fit(X_train, y_train)
#print(select_model_etree.feature_importances_)

#lasso
def pretty_print_linear(coefs, names = None, sort = False):
    if names == None:
        names = ["X%s" % x for x in range(len(coefs))]
    lst = zip(coefs, names)
    if sort:
        lst = sorted(lst,  key = lambda x:-np.abs(x[0]))
    return " + ".join("%s * %s" % (round(coef, 3), name)
                                   for coef, name in lst)

#lasso = Lasso(alpha=.3)
#lasso.fit(X_train, y_train)
#print("Lasso model: ", pretty_print_linear(lasso.coef_, None, sort = True))

#plot roc function
def plot_roc(labels, predict_prob):
    false_positive_rate,true_positive_rate,thresholds=roc_curve(labels, predict_prob)
    roc_auc=auc(false_positive_rate, true_positive_rate)
    plt.title('ROC')
    plt.plot(false_positive_rate, true_positive_rate,'b',label='AUC = %0.4f'% roc_auc)
    plt.legend(loc='lower right')
    plt.plot([0,1],[0,1],'r--')
    plt.ylabel('TPR')
    plt.xlabel('FPR')


'''模型训练 and 调参'''
# train functions
def logistic_regression_training(model_path,x_train,y_train,if_adujst=False,params=None):
    if if_adujst:
        if __name__=='__main__':
            LR = LogisticRegression()
            clf = GridSearchCV(LR,params,cv = 5,scoring = 'neg_log_loss',n_jobs=10,verbose=1)
            clf.fit(x_train, y_train)
            best_estimator = clf.best_estimator_
            print(clf.best_score_)
            print(clf.best_params_)
            save_model(best_estimator, model_path)
    else:  
        clf = LogisticRegressionCV(Cs=5,cv=5,solver='sag',max_iter=200,random_state=0)
        clf.fit(x_train,y_train)
        save_model(clf, model_path)

params_lr = dict(C=[0.001, 0.01, 0.1, 1, 10, 100, 1000],penalty=['l2', 'l1'])

#logistic_regression_training(logistic_model_adjust, X_train_std, y_train,if_adujst=True,params=params_lr)

def svm_training(model_path,x_train,y_train,if_adujst=False,params=None):
    if if_adujst:
        if __name__=='__main__':
            SVM = SVC(kernel='rbf', probability=True)
            clf = GridSearchCV(SVM,params,scoring = 'neg_log_loss',n_jobs=15,verbose=1)
            clf.fit(x_train, y_train)
            best_estimator = clf.best_estimator_
            print(clf.best_score_)
            print(clf.best_params_)
            save_model(best_estimator, model_path)
    else:  
        clf = SVC(kernel='rbf', probability=True)
        clf.fit(x_train,y_train)
        save_model(clf, model_path)  
        
params_svm = dict(C=[0.001, 0.01, 0.1, 1, 10, 100, 1000],gamma=[0.001, 0.0001])

#svm_training(svm_model_adjust, X_train_std, y_train,if_adujst=True,params=params_svm)


def decision_tree_training(model_path,x_train,y_train,if_adujst=False,params=None):
    if if_adujst:
        if __name__=='__main__':
            dt = DecisionTreeClassifier()
            #clf = GridSearchCV(dt,params,scoring = 'neg_log_loss',n_jobs=5,verbose=1)
            clf=RandomizedSearchCV(dt,params,scoring = 'neg_log_loss',n_jobs=5,verbose=1)
            clf.fit(x_train, y_train)
            best_estimator = clf.best_estimator_
            #print(clf.best_score_)
            #print(clf.best_params_)
            write_best_param(best_param_record, 'decision tree', clf.best_params_)            
            save_model(best_estimator, model_path)
    else:  
        clf = DecisionTreeClassifier()
        clf.fit(x_train,y_train)
        save_model(clf, model_path)  

params_dt = dict(max_depth = np.linspace(1, 32, 32, endpoint=True),min_samples_split = np.linspace(0.1, 1.0, 10, endpoint=True),
                min_samples_leaf = np.linspace(0.1, 0.5, 5, endpoint=True),max_features = list(range(1,90)))
                
#decision_tree_training(dt_model_adjust, X_train, y_train,if_adujst=True,params=params_dt)


def random_forest_training(model_path,x_train,y_train,if_adujst=False,params=None):
    if if_adujst:
        if __name__=='__main__':
            dt = RandomForestClassifier()
            #dt = DecisionTreeClassifier()
            #clf = GridSearchCV(dt,params,scoring = 'neg_log_loss',n_jobs=5,verbose=1)
            clf = RandomizedSearchCV(dt,params,scoring = 'neg_log_loss',n_jobs=5,verbose=1)
            clf.fit(x_train, y_train)
            best_estimator = clf.best_estimator_
            #print(clf.best_score_)
            #print(clf.best_params_)
            write_best_param(best_param_record, 'random forest', clf.best_params_)
            save_model(best_estimator, model_path)
    else:  
        clf = RandomForestClassifier()
        clf.fit(x_train,y_train)
        save_model(clf, model_path)

params_rf = dict(max_depth = np.linspace(1, 32, 32, endpoint=True),min_samples_split = np.linspace(0.1, 1.0, 10, endpoint=True),
                min_samples_leaf = np.linspace(0.1, 0.5, 5, endpoint=True),n_estimators=list(range(10,20,1)))

#random_forest_training(rf_model_adjust, X_train, y_train,if_adujst=True,params=params_rf)


def xgboost_training(model_path,x_train,y_train,if_adujst=False,params=None):
    if if_adujst:
        if __name__=='__main__':
            dt = XGBClassifier(subsample=0.8,colsample_bytree=0.8,scale_pos_weight = 1,min_child_weight = 1)
            #clf = GridSearchCV(dt,params,scoring = 'neg_log_loss',n_jobs=5,verbose=1)
            clf = RandomizedSearchCV(dt,params,scoring = 'neg_log_loss',n_jobs=5,verbose=1)
            clf.fit(x_train, y_train)
            best_estimator = clf.best_estimator_
            #print(clf.best_score_)
            #print(clf.best_params_)
            write_best_param(best_param_record, 'xgboost', clf.best_params_)
            save_model(best_estimator, model_path)
    else:  
        clf = XGBClassifier(subsample=0.8,colsample_bytree=0.8,scale_pos_weight = 1,min_child_weight = 1)
        clf.fit(x_train,y_train)
        save_model(clf, model_path)

#params_xgboost = dict(max_depth = np.linspace(4, 8, 5, endpoint=True),gamma=[i/10.0 for i in range(0,5)],min_child_weight = [2,4,6,8,10],
                #min_samples_leaf = np.linspace(0.1, 0.5, 5, endpoint=True),subsample=[i/10.0 for i in range(6,10)],
                #colsample_bytree=[i/10.0 for i in range(6,10)])
params_xgboost = dict(max_depth = [4,5,6,7,8,9,10],gamma=[i/10.0 for i in range(0,5)],min_child_weight = [2,4,6,8,10]
                      ,min_samples_leaf = np.linspace(0.1, 0.5, 5, endpoint=True),subsample=[i/10.0 for i in range(6,10)]
                    ,colsample_bytree=[i/10.0 for i in range(6,10)])
                
#xgboost_training(xgboost_model_adjust, X_train, y_train,if_adujst=True,params=params_xgboost)
#xgboost_training(xgboost_model_20200318, X_train_std, y_train,if_adujst=False,params=params_xgboost)

#test
#LR = joblib.load(logistic_model_adjust)
#SVM = joblib.load(svm_model_adjust)
#dt = joblib.load(dt_model_adjust)
#rf = joblib.load(rf_model_adjust)
#xgboost = joblib.load(xgboost_model_adjust)
xgboost = joblib.load(xgboost_model_20200318)
#LR_y_predict_train=LR.predict(X_train_std)
#LR_y_predict_test=LR.predict(X_test_std)

#SVM_y_predict_train=SVM.predict(X_train_std)
#SVM_y_predict_test=SVM.predict(X_test_std)

#dt_y_predict_train=dt.predict(X_train)
#dt_y_predict_test=dt.predict(X_test)

#rf_y_predict_train=rf.predict(X_train)
#rf_y_predict_test=rf.predict(X_test)

#xgboost_y_predict_train=xgboost.predict(X_train)
#xgboost_y_predict_test=xgboost.predict(X_test)

#plot_roc(y_test, y_predict)
#print('LR model accuracy...')
#print(accuracy_score(y_true=y_train, y_pred=LR_y_predict_train))
#print(accuracy_score(y_true=y_test, y_pred=LR_y_predict_test))

#print('SVM model accuracy...')
#print(accuracy_score(y_true=y_train, y_pred=SVM_y_predict_train))
#print(accuracy_score(y_true=y_test, y_pred=SVM_y_predict_test))

#print('dt model accuracy...')
#print(accuracy_score(y_true=y_train, y_pred=dt_y_predict_train))
#print(accuracy_score(y_true=y_test, y_pred=dt_y_predict_test))

#print('rf model accuracy...')
#print(accuracy_score(y_true=y_train, y_pred=rf_y_predict_train))
#print(accuracy_score(y_true=y_test, y_pred=rf_y_predict_test))

#print('xgboost model accuracy...')
#print(accuracy_score(y_true=y_train, y_pred=xgboost_y_predict_train))
#print(accuracy_score(y_true=y_test, y_pred=xgboost_y_predict_test))
#wrong_index=[index for index in range(len(y_predict)) if list(y_test)[index]!=y_predict[index]]
#wrong_predict_pred_label=[y_predict[index] for index in wrong_index]
#wrong_predict_true_label=[list(y_test)[index] for index in wrong_index]
#print('LR model classification report...')
#print(classification_report(y_train, LR_y_predict_train))
#print(classification_report(y_test, LR_y_predict_test))

#print('SVM model classification report...')
#print(classification_report(y_train, SVM_y_predict_train))
#print(classification_report(y_test, SVM_y_predict_test))

#print('dt model classification report...')
#print(classification_report(y_train, dt_y_predict_train))
#print(classification_report(y_test, dt_y_predict_test))

#print('rf model classification report...')
#print(classification_report(y_train, rf_y_predict_train))
#print(classification_report(y_test, rf_y_predict_test))

#print('xgboost model classification report...')
#print(classification_report(y_train, xgboost_y_predict_train))
#print(classification_report(y_test, xgboost_y_predict_test))



#定义画决策边界的函数
def plot_decision_boundary(pred_func,X):
    # 设定最大最小值，附加一点点边缘填充
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    h = 0.01

    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # 用预测函数预测一下
    Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    # 然后画出图
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    #plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
    
    plt.scatter(x_test_pca[y_test==0,0], x_test_pca[y_test==0,1])
    plt.scatter(x_test_pca[y_test==1,0], x_test_pca[y_test==1,1])
    plt.scatter(x_test_pca[y_test==2,0], x_test_pca[y_test==2,1])
    plt.scatter(x_test_pca[y_test==3,0], x_test_pca[y_test==3,1])    
    



def plot_decision_boundary_new(model, axis):
    # meshgrid函数用两个坐标轴上的点在平面上画格，返回坐标矩阵
    X0, X1 = np.meshgrid(
            # 随机两组数，起始值和密度由坐标轴的起始值决定
                np.linspace(axis[0], axis[1], int((axis[1] - axis[0]) * 10)).reshape(-1, 1),
                np.linspace(axis[2], axis[3], int((axis[3] - axis[2]) * 10)).reshape(-1, 1),
        )
    
    
    #x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    #y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    #h = 0.01

    #X0, X1 = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))    
    # ravel()方法将高维数组降为一维数组，c_[]将两个数组以列的形式拼接起来形成矩阵
    X_grid_matrix = np.c_[X0.ravel(), X1.ravel()]

    # 通过训练好的逻辑回归模型，预测平面上这些点的分类
    y_predict = model.predict(X_grid_matrix)
    y_predict_matrix = y_predict.reshape(X0.shape)

    # 设置色彩表
    from matplotlib.colors import ListedColormap
    my_colormap = ListedColormap(['#0000CD', '#40E0D0', '#FFFF00'])

    # 绘制等高线，并且填充等高区域的颜色
    plt.contourf(X0, X1, y_predict_matrix, linewidth=5, cmap=plt.cm.Spectral)

from sklearn.decomposition import PCA
pca = PCA(n_components=2)
x_train_pca=pca.fit_transform(X_train_std)

x_test_pca=pca.fit_transform(X_test_std)


xgboost_clf = XGBClassifier(subsample=0.8,colsample_bytree=0.8,scale_pos_weight = 1,min_child_weight = 1)
xgboost_clf.fit(x_train_pca,y_train)
#plot_decision_boundary(g(X_test_std),X_test_std)

plot_decision_boundary_new(xgboost_clf, axis=[-10, 6, -10, 10])
plt.xlabel('PCA dim 1')
plt.ylabel('PCA dim 2')
plt.scatter(x_test_pca[y_test==0,0][:100], x_test_pca[y_test==0,1][:100])
plt.scatter(x_test_pca[y_test==1,0][:100], x_test_pca[y_test==1,1][:100])
plt.scatter(x_test_pca[y_test==2,0][:100], x_test_pca[y_test==2,1][:100])
plt.scatter(x_test_pca[y_test==3,0][:100], x_test_pca[y_test==3,1][:100])
plt.show()
print()