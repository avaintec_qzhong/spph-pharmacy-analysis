#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.externals import joblib
from sklearn.preprocessing import StandardScaler

#显示所有列
#pd.set_option('display.max_columns', None)
#显示所有行
#pd.set_option('display.max_rows', None)
#设置value的显示长度为100，默认为50
pd.set_option('max_colwidth',100)

# data direct
data_file='data/diag_features.csv'
kmeans_model_path='models/kmeans_model_whole/'

pharmacy_df=pd.read_csv(data_file)

#特征转换，将字符串类型特征转换为自然数字
career=pharmacy_df['career']
mechanism=pharmacy_df['mechanism']

career_feature={career_cate:list(career.drop_duplicates().dropna()).index(career_cate) for career_cate in list(career.drop_duplicates().dropna())}
mechanism_feature={mechanism_cate:list(mechanism.drop_duplicates().dropna()).index(mechanism_cate) for mechanism_cate in list(mechanism.drop_duplicates().dropna())}
pharmacy_df['career']=pharmacy_df['career'].map(career_feature)
pharmacy_df['mechanism']=pharmacy_df['mechanism'].map(mechanism_feature)


#特征shuffle
#pharmacy_df.sample(frac=1)

for column in list(pharmacy_df.columns[pharmacy_df.isnull().sum() > 0]):
    unk_value=999
    pharmacy_df[column].fillna(unk_value, inplace=True)

feature_df=pharmacy_df.iloc[:,1:]
sc = StandardScaler()
pharmacy_df_sc=sc.fit_transform(feature_df)


#训练k-means模型
clf = KMeans(n_clusters=10)
s = clf.fit(pharmacy_df_sc)
print (s)

#9个中心
print (clf.cluster_centers_)

#每个样本所属的簇
print (clf.labels_)

#用来评估簇的个数是否合适，距离越小说明簇分的越好，选取临界点的簇个数
print (clf.inertia_)

#进行预测
print (clf.predict(pharmacy_df_sc))

#保存模型
joblib.dump(clf , kmeans_model_path+'km_10_c.model')

#载入保存的模型
clf = joblib.load(kmeans_model_path+'km_10_c.model')

#预测
res=clf.predict(pharmacy_df_sc)
res_series=pd.DataFrame(res)
i=1
#将预测结果写入文件
with open('kmeans_results_whole/label_10.csv','w') as f:
    for line in res:
        f.write(str(i)+','+str(line)+'\n')
        i+=1
        #print(line)

print()