


import pandas as pd

def get_map(data,columns_name):
    columns_name_mapping = {value:idx for idx,value in enumerate(set(data[columns_name]))}
    return columns_name_mapping

def columns_to_number(data,columns_list):
    if len(columns_list) == 0:
        print('plaese input the columns name you want to do one hot!')
        return None
    else:
        all_columns_name = data.columns
        for idx,columns_name in enumerate(columns_list):
            if columns_name not in all_columns_name:
                print('please conform your columns name is right,because we cannot find the columns name ',columns_name)
            else:
                columns_name_mapping = get_map(data, columns_name)
                data[columns_name] = data[columns_name].map(columns_name_mapping)
    return data

def judge_isnull(data,columns_name):
    isnull = data[columns_name].isnull().sum()
    if isnull > 0:
        data = data[columns_name].fillna(value='空值')
    return data


def columns_to_one_hot_number(data,columns_list):
    if len(columns_list) == 0:
        print('plaese input the columns name you want to do one hot!')
        return None
    else:
        all_columns_name = data.columns
        for idx,columns_name in enumerate(columns_list):
            if columns_name not in all_columns_name:
                print('please conform your columns name is right,because we cannot find the columns name ',columns_name)
            else:
                # data = judge_isnull(data, columns_name)
                temp = pd.get_dummies(data[columns_name], prefix=columns_name)
                data = data.drop(columns_name, axis=1)
                data = pd.concat([temp, data], axis=1)
    return data

if __name__ == "__main__":
    data = pd.read_excel('./data/data_class_type.xlsx')
    # isnull = data['手术名'].isnull().sum()
    # if isnull > 0:
    #     data = data['手术名'].fillna('空值')
    # return data


    columns_list = ['用药','手术名']
    new_data = columns_to_one_hot_number(data,columns_list)
    new_data.to_excel('./test.xlsx', sheet_name='patient_info',index=False)











